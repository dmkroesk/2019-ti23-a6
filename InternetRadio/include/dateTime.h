#ifndef _dateTime_H
#define _dateTime_H

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
#include <time.h>

/*
 * dateTime.h
 *
 * Created: 19/02/2019 13:56:32
 *  Author: Carlos Cadel
 */ 

extern tm tm_datetime;

void newDateTimeInit(void);
void dateTimeInit(void);
void setHour(int hour);
void setMinutes(int minutes);
void setTime(int hour, int minutes);
void setSeconds(int);
void setTimeUsingTimezone(int hour, int minutes);
void setDay(int day);
void setWDay(int wday);
void setMonth(int month);
void setYear(int year);
void setDate(int day, int month);
int dayofweek(int d, int m, int y);
void setTimeZone(int utcPlus);
char* getDateString(void);
char* getTimeString(void);
char* getFullTimeString(void);
int checkAlarm(void);
int datetime_CheckAlarm(void);
double getTimeZone(void);

#endif
