/*
 * alarm.h
 *
 * Created: 28-Feb-19 10:54:34
 *  Author: svber
 */ 

#ifndef ALARM_H_
#define ALARM_H_
#define ALARMTYPE_STANDARD 0
#define ALARMTYPE_RADIO 1
#define ALARMTYPE_NYAN 2

void setAlarmType(int t);
int getAlarmType(void);
void alarmTick(void);
int checkAlarm(void);

int loadAlarm(char* hour, char* min, char* flags, char alarmNumber);
int saveAlarm(char* flags, char* min, char* hour, char alarmNumber);

void snooze(char);
#endif
