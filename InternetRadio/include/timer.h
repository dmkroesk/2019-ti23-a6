#ifndef TIMER_H
#define TIMER_H

#include <stdio.h>
#include <sys/types.h>

void timerInit(void);
void timerCreate(u_long, void*, void*, u_char);

#endif
