#include "..\..\..\ethernut-4.3.3\nut\include\sys\sock_var.h"
#include <stdio.h>

#ifndef ETHERNET_H
#define ETHERNET_H

extern TCPSOCKET *sock;

FILE* stream;
int metaInterval;
int* ignoredData;
extern int ethInitInet(void);
int connectToStream(void);
extern int ethGetNTPTime(void);
int playStream(void);
extern FILE* GetHTTPRawStream(void);
FILE* GetHTTPRawStreamWithAddress(char* netaddress);
void fillStringWithStreamName(char requestName[]);
void freeBeforeNewSettingsFile(void);
char* streamName;
int streamNameSize;
int streamNameLocLCD;
int connection;
extern int stopStream(void);

#endif
