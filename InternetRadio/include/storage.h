#ifndef HEADER_STORAGE
#define HEADER_STORAGE

extern void StorageSaveConfigAll(int*, int*, int*);

extern void StorageSaveLed(int* led);

extern void StorageLoadLed(int* led);

extern void StorageSaveAlarmOn(int* setting);

extern void StorageLoadAlarmOn(int* setting);

extern void StorageLoadConfigAll(int*, int*, int*);

extern void StorageSaveAlarm1(char* day, char* min, char* hour);

extern void StorageLoadAlarm1(char* day, char* min, char* hour);

extern void StorageSaveAlarm2(char* day, char* min, char* hour);

extern void StorageLoadAlarm2(char* day, char* min, char* hour);

extern void StorageSaveAlarm3(char* day, char* min, char* hour);

extern void StorageLoadAlarm3(char* day, char* min, char* hour);

extern void StorageSaveAlarm4(char* day, char* min, char* hour);

extern void StorageLoadAlarm4(char* day, char* min, char* hour);

extern void StorageSaveAlarm5(char* day, char* min, char* hour);

extern void StorageLoadAlarm5(char* day, char* min, char* hour);

extern void StorageSaveAlarm6(char* day, char* min, char* hour);

extern void StorageLoadAlarm6(char* day, char* min, char* hour);

extern void StorageSaveRadioChar(unsigned char* setting);
extern void StorageLoadRadioChar(unsigned char* setting);

extern void StorageSaveConfigL(int*);

extern void StorageLoadConfigL(int*);

extern void StorageSaveConfigT(int*);

extern void StorageLoadConfigT(int*);

extern void StorageSaveConfigV(int*);

extern void StorageLoadConfigV(int*);

extern void StorageSaveConfigMac(int size);

extern void StorageLoadConfigMac(int size);

#endif
