
/*
 * threadHandling.h
 *
 * Created: 12/03/2019 10:58:29
 *  Author: Carlos Cadel
 */ 

#ifndef _threadHandling_H
#define _threadHandling_H

void thread_handling_wait(int ms);

#endif
