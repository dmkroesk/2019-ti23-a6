/* ========================================================================
* [PROJECT]    SIR
* [MODULE]     Remote Control
* [TITLE]      remote control header file
* [FILE]       remcon.h
* [VSN]        1.0
* [CREATED]    1 july 2003
* [LASTCHNGD]  1 july 2003
* [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
* [PURPOSE]    remote control routines for SIR
* ======================================================================== */

/*-------------------------------------------------------------------------*/
/* global defines                                                          */
/*-------------------------------------------------------------------------*/
#define RC_OK                 0x00
#define RC_ERROR              0x01
#define RC_BUSY               0x04

#define RCST_IDLE             0x00
#define RCST_WAITFORLEADER    0x01
#define RCST_SCANADDRESS      0x02
#define RCST_SCANDATA         0x03

#define RC_INT_SENS_MASK      0x03
#define RC_INT_FALLING_EDGE   0x02
#define RC_INT_RISING_EDGE    0x03

#define IR_RECEIVE            4
#define IR_BUFFER_SIZE        1

/*
* remote control key mapping
*/
typedef enum {
	RC_BUTTON_N1 = 8,
	RC_BUTTON_N2 = 136,
	RC_BUTTON_N3 = 72,
	RC_BUTTON_N4 = 200,
	RC_BUTTON_N5 = 40,
	RC_BUTTON_CHUP = 0,
	RC_BUTTON_UP = 82,
	RC_BUTTON_VOLUP = 64,
	RC_BUTTON_RIGHT = 18,
	RC_BUTTON_VOLDOWN = 192,
	RC_BUTTON_LEFT = 146,
	RC_BUTTON_CHDOWN = 128,
	RC_BUTTON_DOWN = 210,
	RC_BUTTON_POWER = 16,
	RC_BUTTON_BACK = 152,
	RC_BUTTON_AB = 80,
	RC_BUTTON_MUTE = 144,
	RC_NO_BUTTON = -1
} RcKey;

/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/
void RcInit(void);
void printRemoteconInfo(void);
int checkRemoteCode(void);


/*  様様  End Of File  様様様様 様様様様様様様様様様様様様様様様様様様様様様 */













