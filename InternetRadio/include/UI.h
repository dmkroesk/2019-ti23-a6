/*
 * UI.h
 *
 * Created: 19/02/2019 12:57:10
 *  Author: Tim
 */ 


#ifndef UI_H_
#define UI_H_

extern int* MAC;
int closeBeeping;

void uiInit(void);
void showDisplay(void);
void updateVariables(void);
void keyPressEvent(int);
void initShowDateTime(void);
void initSetAlarm(void);
void initSelectTime(void);
int play(FILE *stream);
int dayofweek(int d, int m, int y);
void lcdBackLightActivation(void);
void checkLcdBackLight(void);
void initALARMGOINOFF(void);
void drawAGF(void);
void drawAGFgame(void);
void initALARMGOINOFF(void);


#endif /* UI_H_ */
