
/*
 * weather.h
 *
 * Created: 19/03/2019 14:06:42
 *  Author: Carlos Cadel
 */ 

#ifndef WEATHER_H
#define WEATHER_H

typedef struct {
	char location[16];
	char temperature[8];
	char summary[128];
	char expectation[512];
	char minTemperature[8];
	char maxTemperature[8];
} weather_struct;

extern weather_struct _weather;

int weather_init(void);

#endif
