#include <sys/timer.h>
#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <time.h>
#include "ethernet.h"
#include <sys/socket.h>
#include <dev/board.h>
#include <net/route.h>
#include <pro/sntp.h>
#include <stdlib.h>
#include <stdio.h>
#include <io.h>
#include <string.h>
#include <sys/version.h>
#include <netinet/tcp.h>
#include "ethernet.h"
#include <errno.h>
#include "led.h"
#include "UI.h"
#include "dateTime.h"
#include "UI.h"
#include "threadHandling.h"
#include "storage.h"
#include "vs10xx.h"

#define NOK 1
#define OK 0

#define RADIO_CELTIC 0
#define RADIO_CHRISTMAS 1
#define RADIO_MEDITATION 2
#define RADIO_JAZZ 3

typedef struct {
	unsigned char id;
	unsigned long ips;
	unsigned int port;
} RADIO_STRUCT;

unsigned char currentRadio = 0;

FILE *stream;
TCPSOCKET *sock;
char* streamURLCurrent;
	
	/* 
     * connects to the internet 
     */
int ethInitInet(void)
{
	//uint8_t mac_addr[6] = { 0xC0, 0x01, 0x1E, 0x01, 0x02, 0x03 };
	uint8_t mac_addr[6] = { 0x00, 0x0f, 0xFE, 0x74, 0x13, 0x03 };

	
	int result = OK;

	// Registreer NIC device (located in nictrl.h)
	if( NutRegisterDevice(&DEV_ETHER, 0x8300, 5) )
	{ 
		printf("Error: %s\n", "Error: >> NutRegisterDevice()");
		result = NOK;
	}
	
	if( OK == result )
	{
		if( NutDhcpIfConfig(DEV_ETHER_NAME, mac_addr, 0) )
		{
			printf("Error2: %s\n", "Error: >> NutDhcpIfConfig()");
			result = NOK;
		}
	}
	if( OK == result )
	{
		printf("OK!: %s\nif_name: %s\nip-addr: %s\nip-mask: %s\ngw     : %s\n",
			"Networking setup OK, new settings are:",
			confnet.cd_name,
			inet_ntoa(confnet.cdn_ip_addr),
			inet_ntoa(confnet.cdn_ip_mask),
			inet_ntoa(confnet.cdn_gateway));
	}
	//thread_handling_wait(1000);
	streamURLCurrent = calloc(1,100);
	return result;
}

int ethGetNTPTime()
{
	//Tijdelijke lokale tijdzone, LET OP, GMT +1 wordt in de code -1!!
	double timez = getTimeZone() * -1;
	
	_timezone = timez *60.0*60.0;
	time_t ntp_time = 0;
	tm *ntp_datetime;
	uint32_t timeserver = 0;
	/* Retrieve time from the "pool.ntp.org" server. This is a free NTP server. */
    puts("Retrieving time from 0.nl.pool.ntp.org...");
	
    timeserver = inet_addr("95.211.212.5");
 
    
        if (NutSNTPGetTime(&timeserver, &ntp_time) != 0) {
			//printf("%d", ntp_time);
			thread_handling_wait(1000);
			puts("Failed to retrieve time. Retrying...");
        } else {
			printf("WERKT!!");
			connection = 1;
        }
   
    puts("Done.\n");
	if(connection){
		ntp_datetime = localtime(&ntp_time);
 	
	   printf("NTP time is: %02d:%02d:%02d\n", ntp_datetime->tm_hour, ntp_datetime->tm_min, ntp_datetime->tm_sec);
    
		setTime(ntp_datetime->tm_hour, ntp_datetime->tm_min);
		setSeconds(ntp_datetime->tm_sec);
		setDate(ntp_datetime->tm_mday, ntp_datetime->tm_mon);
		setYear(ntp_datetime->tm_year);
	
		printf("%d/%d/%d\n", ntp_datetime->tm_mday, ntp_datetime->tm_mon, ntp_datetime->tm_year);
	
		setWDay(dayofweek(ntp_datetime->tm_mday, ntp_datetime->tm_mon, ntp_datetime->tm_year));
	}
	
    return 0;
}

FILE* GetHTTPRawStream(void)
{
	RADIO_STRUCT radiostructs[] = {
		{
			RADIO_CELTIC,
			0x8E2C83FD,
			9164
		},{
			RADIO_CHRISTMAS,
			0xA77240B5,
			8232
		},{
			RADIO_MEDITATION,
			0x53A901B6,
			6660
		},{
			RADIO_JAZZ,
			0x2D4FBA7C,
			8443
		},{
			-1,
			-1,
			-1,
		}
	};
	StorageLoadRadioChar(&currentRadio);
	printf("http shite");
	char *data;
	//
	if (sock != NULL)
	{
		NutTcpCloseSocket(sock);
	}
	sock = NutTcpCreateSocket();

	
	char ipadd[16];
	sprintf(ipadd,"%02u.%02u.%02u.%02u", (unsigned int)((radiostructs[currentRadio].ips)>>(8*3)),
	(unsigned int)((radiostructs[currentRadio].ips)>>(8*2))%(0x100),
	(unsigned int)((radiostructs[currentRadio].ips)>>(8*1))%(0x100),
	(unsigned int)((radiostructs[currentRadio].ips))%(0x100));
	printf(ipadd);
	printf(" %lu ",radiostructs[currentRadio].ips);
	printf(" %iu ",radiostructs[currentRadio].port);
	if( NutTcpConnect(	sock,
						inet_addr(ipadd),
						radiostructs[currentRadio].port ))
	{
		printf("Error: >> NutTcpConnect()");
		
		
	}
// 	if( NutTcpConnect(	sock,
// 						inet_addr("45.79.186.124"), 
// 						8443 ))
// 	{
// 		printf("Error: >> NutTcpConnect()");
// 		
// 		
// 	}
	stream = _fdopen( (int) sock, "r+b" );
	
	fprintf(stream, "GET %s HTTP/1.0\r\n", "/");
	fprintf(stream, "Host: %s\r\n", "45.79.186.124");
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
	fprintf(stream, "Icy-MetaData: 1\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);
	
	// Server stuurt nu HTTP header terug, catch in buffer
	data = (char *) malloc(512 * sizeof(char));
	
	while( fgets(data, 512, stream) )
	{
		thread_handling_wait(1);
		
		if( 0 == *data )
		{
			break;
		}
	}
	puts(data);
	
	free(data);
	playStream();
	return stream;
}

int connectToStream(void)
{
	int result = OK;
	char *data;
	
	if (sock != NULL)
	{
		NutTcpCloseSocket(sock);
	}
	
	sock = NutTcpCreateSocket();
	if( NutTcpConnect(	sock,
						inet_addr("192.168.1.142"), 
						8000) )
	{
		printf("Error: >> NutTcpConnect()");
		exit(1);
	}
	stream = _fdopen( (int) sock, "r+b" );
	fprintf(stream, "GET %s HTTP/1.0\r\n", "/");
	fprintf(stream, "Host: %s\r\n", "62.212.132.54");
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);

	
	// Server stuurt nu HTTP header terug, catch in buffer
	data = (char *) malloc(512 * sizeof(char));
	
	while( fgets(data, 512, stream) )
	{
		thread_handling_wait(1);
		
		if( 0 == *data )
		{
			break;
		}
	}
	
	free(data);

	return result;
}

	/* 
     *  activate the music stream received from the internet connection
     */
int playStream(void)
{
	play(stream);
	VsSetVolume(0, 0); //voor nu geluid op max volume, moet nog gefixt worden dat de stream sluit
	return OK;
}

int stopStream(void)
{
	//fclose(stream);
	VsSetVolume(254, 254); //voor nu geluid op 0 volume, moet nog gefixt worden dat de stream sluit
	return OK;
}

	/* 
     *  the creates the file received from the internet
	 * Parameter: the network address of the requested song
	 * return: stream data of the network address that creates a song to play
     */
FILE* GetHTTPRawStreamWithAddress(char* netaddress)
{ 
	char *data;
	if (sock != NULL)
	{
		NutTcpCloseSocket(sock);
	}
    sock = NutTcpCreateSocket();
	char* ip = malloc(23*sizeof(char));
	strncpy(ip, netaddress, 22);
	char nullTerm=0;
	int slashLoc=0;
	int colonLoc=0;
	int i;
	int port = 80;
	char* stringData;
	char* stringStreamNameLoc;
	streamName = "_";

	/*-- Finding the forward slash and port colon --*/
	for(i = 0;i<=23;++i)
	{
		if(ip[i]=='/')
		{
			ip[i]=0;
			nullTerm = 1;
			slashLoc = i;
			printf("slashLoc set to %d\n", slashLoc);
			break;
		}

		if(ip[i]==':')
		{
			colonLoc = i;
			printf("colonLoc set to %d\n", colonLoc);
		}

		else if (ip[i] ==0)
		{
			nullTerm=-1;
			break;
		}
	} 
	if (colonLoc)
	{
		ip[colonLoc] = 0;
		char* Sport = malloc((slashLoc - colonLoc)+2*sizeof(char));
		for (i=colonLoc+1;i<slashLoc;++i)
		{
			Sport[i-colonLoc-1] = netaddress[i];
		}
		Sport[slashLoc - colonLoc-1] = 0;
		
		port = atoi(Sport);
		free(Sport);
	}

	if (!nullTerm)
	{
		ip[17] = 0;
	}   
    //str[strlen(str) - 1] = 0;
    char* address = malloc(80*sizeof(char));
    memset(address, 0, sizeof(address));
    printf("connecting to ip %s\n", ip);
    if( NutTcpConnect(	sock,
						inet_addr(ip), 
						port) )
	{
		printf("Error: >> NutTcpConnect()");
	}
    else
    {
    	//free(ip);
        
        if (nullTerm>0)
        {
	 		for (i=slashLoc;i < 79; ++i)
	        {
	            address[i-slashLoc] = netaddress[i];
	        }
    	}
    	else
    	{
    		address[0] = 0;
    	}	
    	//free(netaddress);
    	//printf("opening %s%s\n", ip, address);
        stream = _fdopen((int) sock, "r+b");
        if (stream == NULL)
        {
        	printf("%s\n", "STREAM IS NULL ABORT!");
        	printf("%d\n",errno );
        	exit(1);
        }
        printf("Address is: %s\n", address);
        fprintf(stream, "GET %s HTTP/1.0\r\n", address);
		fprintf(stream, "Host: %s\r\n", "62.212.132.54");
		fprintf(stream, "User-Agent: Ethernut\r\n");
		fprintf(stream, "Accept: */*\r\n");
		fprintf(stream, "Connection: close\r\n\r\n");
		fflush(stream);
		data = (char *) malloc(512 * sizeof(char));
		
		strncpy(streamName, "", 1);
		while( fgets(data, 512, stream) )
		{
			thread_handling_wait(1);
			
			stringData = strstr(data, "icy-metaint:");
			stringStreamNameLoc = strstr(data, "icy-name:");
			if (strncmp(stringStreamNameLoc,"icy-name:",strlen("icy-name:")) == 0)
			{
				streamName = "";
				strcpy(streamName,strstr(stringStreamNameLoc, ":")+1);
				printf("%s\n",streamName );
				for (streamNameSize = 0; streamNameSize < 16; ++streamNameSize)
				{
					if (streamName[streamNameSize] == 0)
					{
						streamNameSize-=1;
						break;
					}
				}
				streamNameLocLCD = ( 8-(streamNameSize/2));
				
				
			}

			if (stringData != NULL)
			{
				printf("Hoera, gevonden! %s\n", stringData );
				metaInterval = atoi(strstr(stringData,":")+1);
			}
			char* EOT = strstr(data, "\r\n\r\n");
			
			if (EOT != NULL)
			{
				//ignoredData = sizeof(EOT);
				printf("%s\n", EOT);
				break;
			}
			
			if( 0 == *data )
			{
				break;
			}
		}
		
		printf("Stream name: %s\n",streamName);
		if (strcmp(streamName,"_") == 0)
		{
			strcpy(streamName,strstr(address, "/")+1);
			
			for (streamNameSize = 0; streamNameSize < 16; ++streamNameSize)
			{
				if (streamName[streamNameSize] == 0)
				{
					streamNameSize+=1;
					break;
				}
			}
			streamNameLocLCD = ( 8-(streamNameSize/2));
			
		}
		free(address);
		free(ip);
		free(data);
        return stream;
	}
	
	return NULL;
}
