/*! \mainpage SIR firmware documentation
*
*  \section intro Introduction
*  A collection of HTML-files has been generated using the documentation in the sourcefiles to
*  allow the developer to browse through the technical documentation of this project.
*  \par
*  \note these HTML files are automatically generated (using DoxyGen) and all modifications in the
*  documentation should be done via the sourcefiles.
*/

/*! \file
*  COPYRIGHT (C) STREAMIT BV 2010
*  \date 19 december 2003
*/




#define LOG_MODULE  LOG_MAIN_MODULE
//#define USE_JTAG 0

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
//#include <pthread.h>

#include "storage.h"
#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "UI.h"
#include "wificontrol.h"
//#include <UI.h>
#include "dateTime.h"
#include "vs10xx.h"
#include "ethernet.h"
#include <time.h>
#include "rtc.h"
#include "timer.h"
#include "alarm.h"
#include "threadHandling.h"

static void SysMainBeatInterrupt(void*);
static void SysControlMainBeat(u_char);
//int VsSetVolume(u_char left, u_char right);
//int VsBeepStart(u_char fsin);




/* ����������������������������������������������������������������������� */
/*!
* \brief ISR MainBeat Timer Interrupt (Timer 2 for Mega128, Timer 0 for Mega256).
*
* This routine is automatically called during system
* initialization.
*
* resolution of this Timer ISR is 4,448 msecs
*
* \param *p not used (might be used to pass parms from the ISR)
*/
/* ����������������������������������������������������������������������� */
static void SysMainBeatInterrupt(void *p)
{

	/*
	*  scan for valid keys AND check if a MMCard is inserted or removed
	*/
	KbScan();
	CardCheckCard();
}


/* ����������������������������������������������������������������������� */
/*!
* \brief Initialise Digital IO
*  init inputs to '0', outputs to '1' (DDRxn='0' or '1')
*
*  Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
*  is written to the pin (PORTxn='1')
*/
/* ����������������������������������������������������������������������� */
void SysInitIO(void)
{
	/*
	*  Port B:     VS1011, MMC CS/WP, SPI
	*  output:     all, except b3 (SPI Master In)
	*  input:      SPI Master In
	*  pull-up:    none
	*/
	outp(0xF7, DDRB);

	/*
	*  Port C:     Address bus
	*/

	/*
	*  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
	*  output:     Keyboard colums 2 & 3
	*  input:      LCD_data, SDA, SCL (TWI)
	*  pull-up:    LCD_data, SDA & SCL
	*/
	outp(0x0C, DDRD);
	outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

	/*
	*  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
	*  output:     CS Flash, LCD BL/Enable, USB Tx
	*  input:      VS1011 (DREQ), RTL8019, IR
	*  pull-up:    USB Rx
	*/
	outp(0x8E, DDRE);
	outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

	/*
	*  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
	*  output:     LCD RS/RW, LED
	*  input:      Keyboard_Rows, MCC-detect
	*  pull-up:    Keyboard_Rows, MCC-detect
	*  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
	*/
	#ifndef USE_JTAG
	sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
	sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256

	
	#endif //USE_JTAG

	cbi(OCDR, IDRD);
	cbi(OCDR, IDRD);


	outp(0x0E, DDRF);
	outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

	/*
	*  Port G:     Keyboard_cols, Bus_control
	*  output:     Keyboard_cols
	*  input:      Bus Control (internal control)
	*  pull-up:    none
	*/
	outp(0x18, DDRG);
}

/* ����������������������������������������������������������������������� */
/*!
* \brief Starts or stops the 4.44 msec mainbeat of the system
* \param OnOff indicates if the mainbeat needs to start or to stop
*/
/* ����������������������������������������������������������������������� */
static void SysControlMainBeat(u_char OnOff)
{
	int nError = 0;

	if (OnOff==ON)
	{
		nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
		if (nError == 0)
		{
			init_8_bit_timer();
		}
	}
	else
	{
		// disable overflow interrupt
		disable_8_bit_timer_ovfl_int();
	}
}

//Er wordt al een thread aangemaakt in de ethernet klasse nu is er sprake van een thread in een thread.
// Misschien deze even weghalen.
THREAD(internetThread, arg) //CustomThread
{
	VsSetVolume(0,0);

	ethInitInet();

	ethGetNTPTime();
	printf("Almost Ready!!!!");
	socketInit();
	unsigned char val = 0;
	StorageLoadRadioChar(&val);
	
	if (val > 3) val = 0;
	
	
	StorageSaveRadioChar(&val);
	//GetHTTPRawStream();
	
	
	NutThreadExit();
	while(1){thread_handling_wait(1);}
}

/* ����������������������������������������������������������������������� */
/*!
* \brief Main entry of the SIR firmware
*
* All the initialisations before entering the for(;;) loop are done BEFORE
* the first key is ever pressed. So when entering the Setup (POWER + VOLMIN) some
* initialisatons need to be done again when leaving the Setup because new values
* might be current now
*
* \return \b never returns
*/
/* ����������������������������������������������������������������������� */
int main(void)
{
	/*
	*  First disable the watchdog
	*/
	WatchDogDisable();
	thread_handling_wait(100);
	
	
	/*
	* Initialize drivers, settings and other specifications necessary for start-up
	*/
	SysInitIO();
	SPIinit();
	LedInit();
	LcdLowLevelInit();
	Uart0DriverInit();
	Uart0DriverStart();
	
	LogInit();
	
	
	
	CardInit();
	X12Init();	//CLOCK
	VsPlayerInit();
	int vol = 0;
	StorageLoadConfigV(&vol);
	VsSetVolume((vol - 250)* -1, (vol - 250)* -1);
	customCharacters();
	if (At45dbInit()==AT45DB041B)
	{// ......
	}
	RcInit();
	KbInit();
	uiInit();

	NutThreadCreate("internetThread", internetThread, NULL, 512);
	LogMsg_P(LOG_INFO, PSTR("Hello world!"));
	SysControlMainBeat(ON);             // enable 4.4 msecs hartbeat interrupt
	
	/*
	* Increase our priority so we can feed the watchdog.
	*/
	NutThreadSetPriority(1);
	
	initShowDateTime();
	//initALARMGOINOFF();
	
	//VsBeepStart(1);

	//VsPlayerKick();
	//initSelectTime();
	//showDisplay();
	//VsBeep();
	int t = 0;
	int tempMinute = -1;
	int led;
	StorageLoadLed(&led);
	if(0 == led){
		LedControl(LED_ON);
	}
	else{
		LedControl(LED_OFF);
	}

	int key= KEY_UNDEFINED;
	srand(tm_datetime.tm_min);
	while(1)
	{
		t++;
		thread_handling_wait(1);
						
		int remotekey = checkRemoteCode();
		if (remotekey!= KEY_UNDEFINED)
		{
			keyPressEvent(remotekey);
		}
		
		key = KbGetKey();
		 		
		
		keyPressEvent(key);
		
		checkLcdBackLight();
		
		if(tempMinute != tm_datetime.tm_min)// && connection)
		{	
			int alarmState;
		

			if(0 == tm_datetime.tm_sec){
				alarmState = 0;
				StorageSaveAlarmOn(&alarmState);
			}
				

			tempMinute = tm_datetime.tm_min;

			
			StorageLoadAlarmOn(&alarmState);
			if(2 != alarmState){
				alarmTick();
			}
			else{
				alarmState = 0;
				StorageSaveAlarmOn(&alarmState);
			}
		}
		
	}


	return(1);
}

/* ---------- end of module ------------------------------------------------ */
/*@}*/

