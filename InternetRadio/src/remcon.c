/*! \file
* remcon.c contains all interface- and low-level routines that
* perform handling of the infrared bitstream
* [COPYRIGHT]  Copyright (C) STREAMIT BV
*  \version 1.0
*  \date 26 september 2003
*/



#define LOG_MODULE  LOG_REMCON_MODULE
#define LENGTH_LEAD_MIN 2900
#define LENGTH_LEAD_MAX 3300
#define LENGTH_BIT_HIGH_MIN 400
#define LENGTH_BIT_HIGH_MAX 600
#define LENGTH_BIT_LOW_MIN  200
#define LENGTH_BIT_LOW_MAX  300

#include <stdlib.h>
#include <fs/typedefs.h>
#include <sys/heap.h>

#include <sys/event.h>
#include <sys/atom.h>
#include <sys/types.h>
#include <dev/irqreg.h>

#include "system.h"
#include "portio.h"
#include "remcon.h"
#include "display.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"


//NEC protocol remote code: 057

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
static HANDLE  hRCEvent;
int irStarted = 0;
int currentBit = 1;
int currentCode = 0;
int finalCode = 0;
uint16_t timePrevInterrupt = 0;

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void RcInterrupt(void*);
static void RcClearEvent(HANDLE*);
void RcBitReceived(int);
int RcKeyToKbKey(int);
void printRemoteconInfo(void);

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/* brief: logging the code received from the remote*/
void printRemoteconInfo(void) {
	LogMsg_P(LOG_INFO, PSTR("Lastreceived code: %d"), finalCode);
}

/*!
* \brief ISR Remote Control Interrupt (ISR called by Nut/OS)
*
*  NEC-code consists of 5 parts:
*
*  - leader (9 msec high, 4,5 msec low)
*  - address (8 bits)
*  - inverted address (8 bits)
*  - data (8 bits)
*  - inverted data (8 bits)
*
*  The first sequence contains these 5 parts, next
*  sequences only contain the leader + 1 '0' bit as long
*  as the user holds down the button
*  repetition time is 108 msec in that case
*
*  Resolution of the 16-bit timer we use here is 4,3 usec
*
*  13,5 msecs are 3109 ticks
*  '0' is 1,25 msecs (260 ticks)
*  '1' is 2,25 msecs (517 ticks)
*
* \param *p not used (might be used to pass parms from the ISR)
*/
static void RcInterrupt(void *p)
{
	uint16_t timer = TCNT1;
	uint16_t difference = timer - timePrevInterrupt;  //Time between last interupt and now.
	if(!irStarted) {
		if(difference > LENGTH_LEAD_MIN && difference < LENGTH_LEAD_MAX) {
			irStarted = 1; // Leader received, listening to IR-code now.
		}
		} else {
		//LedControl(LED_TOGGLE);
		if(difference > LENGTH_BIT_HIGH_MIN && difference < LENGTH_BIT_HIGH_MAX) {
			RcBitReceived(1); // Bit 1 received.
			} else if(difference > LENGTH_BIT_LOW_MIN && difference < LENGTH_BIT_LOW_MAX) {
			RcBitReceived(0); // Bit 0 received.
		}
		//LedControl(LED_TOGGLE);
	}
	timePrevInterrupt = timer; // Set last time interupted to now.
}

void RcBitReceived(int bit) {
	if(currentBit > 16 && currentBit < 25) { // If received bit is a data bit, add to code.
		if(bit) {
			currentCode += (1 << (24 - currentBit)); // plak bit aan elkaar
		}
		//LedControl(LED_TOGGLE);
		} else if(currentBit >= 32) { // If bit is the last bit of the code or the final pulse, Inject the key into the system.
		irStarted = 0;
		currentBit = 0;
		finalCode = currentCode;
		currentCode = 0;
		//KeyboardInjectKey(RcKeyToKbKey(finalCode)); // Remap RC key to KB key, als inject into system.
		//LedControl(LED_OFF);
	}
	currentBit++;
	//LedControl(LED_TOGGLE);
}

/*!
* \brief Clear the eventbuffer of this module
*
* This routine is called during module initialization.
*
* \param *pEvent pointer to the event queue
*/
static void RcClearEvent(HANDLE *pEvent)
{
	NutEnterCritical();
	*pEvent = 0;
	NutExitCritical();
}


/*!
* \brief Initialise the Remote Control module
*
* - register the ISR in NutOS
* - initialise the HW-timer that is used for this module (Timer1)
* - initialise the external interrupt that inputs the infrared data
* - flush the remote control buffer
* - flush the eventqueue for this module
*/
void RcInit()
{
	int nError = 0;

	EICRB &= ~RC_INT_SENS_MASK;    // clear b0, b1 of EICRB

	// Install Remote Control interrupt
	nError = NutRegisterIrqHandler(&sig_INTERRUPT4, RcInterrupt, NULL);
	if (nError == FALSE)
	{
		/*
		*  ToDo: control External Interrupt following NutOS calls
		#if (NUTOS_VERSION >= 421)
		NutIrqSetMode(&sig_INTERRUPT4, NUT_IRQMODE_FALLINGEDGE);
		#else
		EICRB |= RC_INT_FALLING_EDGE;
		#endif
		EIMSK |= 1<<IRQ_INT4;         // enable interrupt
		*/
		EICRB |= RC_INT_FALLING_EDGE;
		EIMSK |= 1<<IRQ_INT4;         // enable interrupt
		} else {
		LogMsg_P(LOG_ALERT, PSTR("remcon.c 179 Can't register Irq handler"));
	}

	// Initialise 16-bit Timer (Timer1)
	TCCR1B |= (1<<CS11) | (1<<CS10); // clockdivider = 64
	TIFR   |= 1<<ICF1;
	//TIMSK = 1<<TICIE1;

	RcClearEvent(&hRCEvent);
}

/*
* Remaps RemoteControl key codes to the corresponding KeyBoard key codes
*/
int RcKeyToKbKey(int RcKey) {
	switch(RcKey) {
		case RC_BUTTON_N1 : return KEY_01;
		case RC_BUTTON_N2 : return KEY_02;
		case RC_BUTTON_N3 : return KEY_03;
		case RC_BUTTON_N4 : return KEY_04;
		case RC_BUTTON_N5 : return KEY_05;
		case RC_BUTTON_CHUP : return KEY_UP;
		case RC_BUTTON_UP : return KEY_UP;
		case RC_BUTTON_VOLUP : return KEY_RIGHT;
		case RC_BUTTON_RIGHT : return KEY_RIGHT;
		case RC_BUTTON_VOLDOWN : return KEY_LEFT;
		case RC_BUTTON_LEFT : return KEY_LEFT;
		case RC_BUTTON_CHDOWN : return KEY_DOWN;
		case RC_BUTTON_DOWN : return KEY_DOWN;
		case RC_BUTTON_POWER : return KEY_POWER;
		case RC_BUTTON_BACK : return KEY_ESC;
		case RC_BUTTON_AB : return KEY_OK;
		case RC_BUTTON_MUTE : return KEY_ALT;
		
		default : return KEY_UNDEFINED;
	}
}

int checkRemoteCode(void)
{
	int KBCode = RcKeyToKbKey(finalCode);
	finalCode = -1;
	return KBCode;
}


/* ---------- end of module ------------------------------------------------ */

/*@}*/
