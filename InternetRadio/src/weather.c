
/*
 * weather.c
 *
 * Created: 19/03/2019 14:06:54
 *  Author: Carlos Cadel
 */ 
#define LOG_MODULE  LOG_MAIN_MODULE
#define JSON_LENGTH 2048
#define MAX_TOKEN_COUNT 128

#include <sys/socket.h>
#include <sys/confnet.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/bankmem.h>
#include <sys/heap.h>

#include <arpa/inet.h>
#include <dev/nicrtl.h>
#include <pro/dhcp.h>
#include <net/route.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "threadHandling.h"
#include "ethernet.h"
#include "jsmn.h"
#include "weather.h"

TCPSOCKET *sock;
FILE *stream;

char buff[JSON_LENGTH];
char json[JSON_LENGTH];
char value[JSON_LENGTH];
char key[JSON_LENGTH];
weather_struct _weather = (weather_struct) {"null", "null", "null", "null", "null", "null"};
jsmn_parser p;
jsmntok_t t[MAX_TOKEN_COUNT];

void weather_on_update(void);
int weather_should_update(void);
int weather_connect_to_weather(void);

int weather_init(void)
{
	printf("\n[weather.c] Connect to weather...\n");	
	
	return weather_connect_to_weather();
}

int weather_jsoneq(const char *json, jsmntok_t *tok, const char *s) 
{
	if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
		strncmp(json + tok->start, s, tok->end - tok->start) == 0) 
	{
		return 0;
	}
	
	return -1;
}

int weather_parse_json(char* json)
{
	int r;
	
	jsmn_init(&p);
	r = jsmn_parse(&p, json, strlen(json), t, sizeof(t)/sizeof(t[0]));
	if(r < 0)
	{
		return 0;
	}
	
	if(r < 1 || t[0].type != JSMN_OBJECT)
	{
		return 0;
	}
	
	
	int i;
	int j;
	
	for (i = 1; i < r; i++)
	{
		if(weather_jsoneq(json, &t[i], "liveweer") == 0)
		{
			
			if (t[i+1].type != JSMN_ARRAY) 
			{
				continue; 
			}
			
			for (j = 0; j < t[i+1].size; j++) 
			{
				jsmntok_t *g = &t[i+j+2];
				
				for (j = 1; j < r; j++)
				{

					jsmntok_t json_value = g[j+1];
					jsmntok_t json_key = g[j];


					int string_length = json_value.end - json_value.start;
					int key_length = json_key.end - json_key.start;

					int idx;

					for (idx = 0; idx < string_length; idx++){
						value[idx] = json[json_value.start + idx ];
					}

					for (idx = 0; idx < key_length; idx++){
						key[idx] = json[json_key.start + idx];
					}

					value[string_length] = '\0';
					key[key_length] = '\0';

					if(strcmp(key, "plaats") == 0)
					{
						strcpy(_weather.location, value);
					}
					else if(strcmp(key, "temp") == 0)
					{
						strcpy(_weather.temperature, value);
					}
					else if(strcmp(key, "samenv") == 0)
					{
						strcpy(_weather.summary, value);
					}
					else if(strcmp(key, "verw") == 0)
					{
						strcpy(_weather.expectation, value);
					}
					else if(strcmp(key, "d0tmax") == 0)
					{
						strcpy(_weather.maxTemperature, value);
					}
					else if(strcmp(key, "d0tmin") == 0)
					{
						strcpy(_weather.minTemperature, value);
					}

					j++;
				}
				
			}
			i += t[i+1].size + 1;
		}
	}
	
	return 1;
}

/*
	MAIN FUNCTION
    Function to connect to api and retrieve data. Also parses it and sets flag for shouldupdate.
*/
int weather_connect_to_weather(void) 
{  
    int timeOutValue = 15000;
	int segmentation = 1460;
	int returnBuffer = 8192;

    //Connecting to api
	if (sock != NULL)
	{
		NutTcpCloseSocket(sock);
	}
	sock = NutTcpCreateSocket();
	
    NutTcpSetSockOpt(sock, 0x02, &segmentation, sizeof(segmentation));
	NutTcpSetSockOpt(sock, SO_RCVTIMEO, &timeOutValue, sizeof(timeOutValue));	
	NutTcpSetSockOpt(sock, SO_RCVBUF, &returnBuffer, sizeof(returnBuffer));
	
   if(NutTcpConnect(sock, inet_addr("79.170.88.194"), 80) == -1)
   {
	   int error = NutTcpError(sock);
	   printf("[weather.c] NutTcpConnect() error: %d\n", error);
	   thread_handling_wait(100);
	   return 0;
   }

	stream = _fdopen((int) sock, "r+b");

	fprintf(stream, "GET %s HTTP/1.0\r\n", "/api/json-data-10min.php?key=f6904044f6&locatie=Breda");
    fprintf(stream, "Host: %s\r\n", "weerlive.nl");
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
    fprintf(stream, "Connection: close\r\n\r\n");
    fflush(stream);

	//Read buffer
    while (fgets(buff, sizeof(buff), stream)) {
		if(strncmp(buff, "{", 1) == 0) //If json string, copy it in json char array
		{
			strcpy(json, buff);
		}
		thread_handling_wait(1);
    }

    //Parsing json
	weather_parse_json(json);

    //closing stream and socket
    fclose(stream);
    NutTcpCloseSocket(sock);
    return 1;
}
