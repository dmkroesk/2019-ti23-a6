/*
* alarm.c
*
* Created: 28-Feb-19 10:54:22
*  Author: svber
*/
#include <stdio.h>
#include <time.h>
#include <sys/thread.h>
//#include <vs10xx.h>
#include "rtc.h"
#include "dateTime.h"
#include "storage.h"
#include "display.h"
#include "UI.h"
#include "ethernet.h"
#include "vs10xx.h"
#include "threadHandling.h"
#include "mmc.h"


#define  BIT(x) (1<<x)

#define ALARMTYPE_STANDARD 0
#define ALARMTYPE_RADIO 1
#define ALARMTYPE_NYAN 2

int currentAlarmType = 1;


int getAlarmType(void){
	return currentAlarmType;
}
/**
 * \brief sets alarmtype
 * 
 * \param t alarmtype
 * 
 * \return void
 */
void setAlarmType(int t){
	currentAlarmType = t;
}
int loadAlarm(char* hour, char* min, char* flags, char alarmNumber);
/*
returns:

*/



THREAD(AlarmBeep, arg) //CustomThread
{ int alarmVolume = VsGetVolume();
	int switching = 1;
	for(;;)
	{
		VsBeepSelfMade(50);
		
		
		if (closeBeeping == 1)
		{
			VsSetVolume(alarmVolume, alarmVolume);
			
			VsBeepSelfMade(50);
			NutThreadExit();
		}
		if (switching)
		{
			VsSetVolume(alarmVolume, 254);
			switching = 0;
		}
		else
		{
			VsSetVolume(254, alarmVolume);
			switching = 1;
		}
		thread_handling_wait(50);
	}
}

void alarmTick(void){
	int alarmCheck = checkAlarm();
	if(alarmCheck != -1)
	{	
		int alarmOn = 1;
		StorageSaveAlarmOn(&alarmOn);
		initALARMGOINOFF();
		LcdBackLight(LCD_BACKLIGHT_ON);
		drawAGFgame();
		showDisplay();
		
		VsSetVolume(0, 0); //moet nog veranderd worden!
		if (!connection)
		{
			setAlarmType(ALARMTYPE_STANDARD);
		}
		
		if (currentAlarmType == ALARMTYPE_RADIO)
		{
			GetHTTPRawStream();
		}
		else if (currentAlarmType == ALARMTYPE_NYAN)
		{
			playNyanCat();
		}
		else{
			//shortBeeb();
			
			NutThreadCreate("Bg", AlarmBeep, NULL, 512);
		}
	}
}

/**
 * checks if alarm is going off
 * 
 * \return the number of the alarm 0-6, -1 if no alarm triggers
 */
int checkAlarm(void)
{
	X12RtcGetClock(&tm_datetime);
	
	char days;
	char hour;
	char min;
	
	int i;
	for (i = 0; i < 6; i++)
	{
		loadAlarm(&hour,&min,&days,i);
		
		if(hour == tm_datetime.tm_hour &&
		min == tm_datetime.tm_min &&
		CHECK_BIT(days,(6-tm_datetime.tm_wday)) != 0)
		{
			return i;
		}
	}
	
	return -1;
}

/*
* \brief saves an alarm in flash memory
*
* \param hour	hour of the time at wich the alarm triggers
* \param min	minutes of the time at wich the alarm triggers
* \param days	flags about the behaviour of the alarm
*		bit 8:	 this represents if the alarm is snoozable
*		bit 7-1: these represent the weekdays at wich the alarm triggers starting with sunday
* \param alarmNumberFlag determins the alarm number and if the alarm repeats
*		bit 8:   is the alarm repeatable
*		bit 7-1: alarm number
*
*/
int saveAlarm(char* flags, char* min, char* hour, char alarmNumber)
{
	
	alarmNumber = abs(alarmNumber);
	switch (alarmNumber)
	{
		case 0:{
			StorageSaveAlarm1(flags,min,hour);
			break;
		}
		
		case 1:{
			StorageSaveAlarm2(flags,min,hour);
			break;
		}
		
		case 2:{
			StorageSaveAlarm3(flags,min,hour);
			break;
		}
		
		case 3:{
			StorageSaveAlarm4(flags,min,hour);
			break;
		}
		
		case 4:{
			StorageSaveAlarm5(flags,min,hour);
			break;
		}
		
		default:{
			return -1;
		}
	}
	
	return 0;
}

/**
 * \brief loads alarm from storage
 * 
 * \param hour
 * \param min
 * \param flags
 * \param alarmNumber
 * 
 * \return 0 if succes, -1 if not succes
 */
int loadAlarm(char* hour, char* min, char* flags, char alarmNumber)
{
	switch ((int)alarmNumber)
	{
		case 0:{
			StorageLoadAlarm1(flags,min,hour);
			break;
		}
		
		case 1:{
			StorageLoadAlarm2(flags,min,hour);
			break;
		}
		
		case 2:{
			StorageLoadAlarm3(flags,min,hour);
			break;
		}
		
		case 3:{
			StorageLoadAlarm4(flags,min,hour);
			break;
		}
		
		case 4:{
			StorageLoadAlarm5(flags,min,hour);
			break;
		case 5:{
			StorageLoadAlarm6(flags,min,hour);
			break;
		}
		}
		
		default:{
			return -1;
		}
		
	}
	
	return 0;
}

/**
 * \brief function for snoozing
 * 
 * \param snoozeLength length of snooze
 * 
 * \return void
 */
void snooze (char snoozeLength )
{
	tm tm_newAlarm;
	
	X12RtcGetClock(&tm_newAlarm);
	
	tm_newAlarm.tm_min += snoozeLength;
	
	if(tm_newAlarm.tm_min > 59)
	{
		tm_newAlarm.tm_min -= 60;
		tm_newAlarm.tm_hour += 1;
	}
	
	if (tm_newAlarm.tm_hour > 23)
	{
		tm_newAlarm.tm_hour -= 24;
	}
	char day = 0b11111111;
	char hour = tm_newAlarm.tm_hour;
	char min = tm_newAlarm.tm_min;
	
	StorageSaveAlarm6(&day,&min,&hour);
	
}
