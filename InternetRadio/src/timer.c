/*
 * timer.c
 *
 * Created: 28/02/2019 10:51:05
 *  Author: Carlos Cadel
 */ 
/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
//#include <pthread.h>

#include "storage.h"
#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "UI.h"
//#include <UI.h>
#include "dateTime.h"
#include "vs10xx.h"

#include <time.h>
#include "rtc.h"

#define SAMPLE_PERIOD		1	//[ms]
#define TIMER1_DIVISOR		14745600/(1000/SAMPLE_PERIOD)	//NUT_CPU_FREQ/...

uint32_t CPU_FREQ;

void Timer1Init(void);

void timerInit(void)
{
	NutTimerInit();
	CPU_FREQ = NutGetCpuClock();
}

void timerCreate(u_long ms, void (*callback) (HANDLE, void *), void * arg, u_char flags)
{
	NutTimerStart(ms, callback, arg, flags);
}
