
/*
 * Sound.c
 *
 * Created: 28-2-2019 09:21:26
 *  Author: mauri
 */ 
#include <sys/heap.h>
#include <sys/bankmem.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include "Sound.h"
#include "vs10xx.h"
#include "log.h"
#include "threadHandling.h"

#define LOG_MODULE  LOG_VS10XX_MODULE

int closeStream;
int fallingAsleepMode;
int fallingAsleepTime;
//volume variables

void shortBeeb(void);


int ProcessMetaData(FILE *stream)
{
    u_char blks = 0;
    u_short cnt;
    int got;
    int rc = 0;
    u_char *mbuf;

    /*
     * Wait for the lenght byte.
     */
    got = fread(&blks, 1, 1, stream);
    if(got != 1) {
        return -1;
    }
    if (blks) {
        if (blks > 32) {

            return -1;
        }

        cnt = blks * 16;
        if ((mbuf = malloc(cnt + 1)) == 0) {
            return -1;
        }

        /*
         * Receive the metadata block.
         */
        for (;;) {
			thread_handling_wait(1);
			
            if ((got = fread(mbuf + rc, 1, cnt, stream)) <= 0) {
                return -1;
            }
            if ((cnt -= got) == 0) {
                break;
            }
            rc += got;
            mbuf[rc] = 0;
        }


        free(mbuf);
    }
    return 0;
}

THREAD(PlayMp3Streamer, arg)//FILE *stream, u_long metaint) //CustomThread
{
	FILE *stream = (FILE *) arg;
    size_t rbytes;
    char *mp3buf;
    char iflag;
    int got = 0;
    //u_long last;
    //u_long mp3left = metaint;

    /*
     * Initialize the MP3 buffer. The NutSegBuf routines provide a global
     * system buffer, which works with banked and non-banked systems.
     */
    if (NutSegBufInit(8192) == 0) 
	{
        puts("Error: MP3 buffer init failed");
        NutThreadExit();
		while(1){thread_handling_wait(1);} //----------------------check if good solution
    }

    /* 
     * Initialize the MP3 decoder hardware.
     */
    if (VsPlayerInit() || VsPlayerReset(0)) 
	{
        puts("Error: MP3 hardware init failed");
		NutThreadExit();
        while(1){thread_handling_wait(1);} //----------------------check if good solution
    }
    //VsSetVolume(0, 0);

    /* 
     * Reset the MP3 buffer. 
     */
	
	        iflag = VsPlayerInterrupts(0);
	        NutSegBufReset();
	        VsPlayerInterrupts(iflag);
    //last = NutGetSeconds();

    for (;;) 
	{
		thread_handling_wait(1);
        /*
         * Query number of byte available in MP3 buffer.
         */
        iflag = VsPlayerInterrupts(0);
        mp3buf = NutSegBufWriteRequest(&rbytes);
        VsPlayerInterrupts(iflag);

        /*
         * If the player is not running, kick it.
         */
        if (VsGetStatus() != VS_STATUS_RUNNING) 
		{
            //if(rbytes < 1024 || NutGetSeconds() - last > 4UL) {
                //last = NutGetSeconds();
                puts("Kick player");
                VsPlayerKick();
            //}
        }

        /* 
         * Do not read pass metadata. 
         */
        //if (metaint && rbytes > mp3left) {
            //rbytes = mp3left;
        //}

        /* 
         * Read data directly into the MP3 buffer. 
         */
        while (rbytes && !closeStream) 
		{
            if ((got = fread(mp3buf, 1, rbytes, stream)) > 0)
			 {
                iflag = VsPlayerInterrupts(0);
                mp3buf = NutSegBufWriteCommit(got);
                VsPlayerInterrupts(iflag);

                //if (metaint) {
                    //mp3left -= got;
                    //if (mp3left == 0) {
                        //ProcessMetaData(stream);
                        //mp3left = metaint;
                    //}
                //}

                if(got < rbytes && got < 512) 
				{
                   
                    thread_handling_wait(250);
                }
                else
                {
					thread_handling_wait(1);
	                //NutThreadYield();
                }
            }
			else 
			{
                break;
            }
            rbytes -= got;


			if(got <= 0)
			{
				break;
			}
		}
    
	        if(closeStream)
	        {
		        closeStream = 0;
				NutThreadExit();
				while(1){thread_handling_wait(1);}
	        }
	}
}


void setCloseStream(int value)
{
	closeStream = value;
}



//void shortBeeb(void)
//{
	//VsBeepSelfMade(50);
//}
//
//int play(FILE *stream)
//{
	//NutThreadCreate("Bg", PlayMp3Streamer, stream, 512);
	//
	//return 1;
//}
