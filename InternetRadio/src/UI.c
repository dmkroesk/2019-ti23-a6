/*
* UI.c
*
* Created: 19/02/2019 12:56:22
*  Author: Tim
*/
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "log.h"
#include "led.h"
#include "weather.h"
#include <stdio.h>
#include <string.h>
#include "storage.h"

#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include "dateTime.h"
#include "display.h"
#include "keyboard.h"
#include "watchdog.h"
#include "vs10xx.h"
#include "alarm.h"
#include "ethernet.h"
#include "Sound.h"
#include "threadHandling.h"
#include "rtc.h"
#include "weather.h"

//define update key
#define KEY_UPDATE 20
#define MAC_ARRAY_SIZE 6
#define LOG_MODULE  LOG_MAIN_MODULE

//standard variables
char topString[16] = "                ";
char botString[16] = "                ";
unsigned int resetTimer;
unsigned int blockDoubleThread;
enum selected {Alarmen,tijd,datum,radio,taal,volumeSettings,MAC_addres,sas,led,weather,reset};
enum selected menuPos = Alarmen;
int menuItems = 11;
weather_struct _weather;
tm tm_datetime;
char lcdTime;
char ledTime;
int botWrite = 1;
char min = 0;
char hour = 0;
int day = 25;
int month = 9;
int year = 1997;
int daysMonth[12] ={31,28,31,30,31,30,31,30,31,30,31,30};
int volumes[11] = {100,130,160,180,200,210,220,230,235,240,245};
#define OK			1
#define NOK			0

int language = LANGUAGE_EN;

int closeStream;
int closeBeeping = 0;
int fallingAsleepMode;
int fallingAsleepTime;

int lastPressedKey = KEY_UNDEFINED;
//volume variables
int volume;

//stuff
int UIHandleReset(void);
void minPlus(void);
void hourMinus(void);
void hourPlus(void);
void minMinus(void);
void selectTime(int key);
void stfillStrings2(void);
void timezoneMinus(void);
void timezonePlus(void);
void keyEventsDateTime(int key);
void saFillStrings(void);
void initShowDateTime(void);
void setSettings(int key);
void drawMenuItem(int top, int sel);
void drawMenuSettings(void);
void initSettingsMenu(void);
void initSelectTime(void);
void initSelectTime(void);
void stfillStrings3(void);
void initSetClock(void);
void drawSetDate(void);
void initSetDate(void);
void setSetDate(int key);
void dayCheck(void);
void initSetAlarm(void);
void stdFillStringsPlaceholder(void);

void saLeftKey(void);
void saRightKey(void);
void saUpKey(void);
void saDownKey(void);
void saEscKey(void);
void saOkKey(void);
void sa1Key(void);
void sa2Key(void);
void sa3Key(void);
void sa4Key(void);
void sa5Key(void);

void sdtAltKey(void);
void sdtDefaultKey(void);

void stLeftKey(void);
void stRightKey(void);
void stUpKey(void);
void stDownKey(void);
void stOkKey(void);
void stEscKey(void);

void drawDefault(void);
void initDefault(void);
void defDownKey(void);
void defEscKey(void);
void defOkKey(void);
void defUpKey(void);

void smDownKey(void);
void smUpKey(void);
void smEscKey(void);
void smOkKey(void);

void initVolume(void);
void drawVolume(void);
void vDownKey(void);
void vUpKey(void);
void vOkKey(void);
void vEscKey(void);

void initNetworkInfo(void);
void drawNi(void);
void niEscKey(void);

void sdDownKey(void);
void sdUpKey(void);
void sdLeftKey(void);
void sdRightKey(void);
void sdEscKey(void);
void sdOkKey(void);

void setVolume(void);

void showDisplay(void);
void showInternetIcon(void);

void initMacAddress(void);
void drawMacAddress(void);
void maLefKey(void);
void maRightKey(void);
void maDownKey(void);
void maUpKey(void);
void maOkKey(void);
void maEscKey(void);

void AGFSnoozeKey(void);
void AGF1Key(void);
void AGF2Key(void);
void AGF3Key(void);
void AGF4Key(void);
void AGF5Key(void);
void AGFAltKey(void);
void AGFDownKey(void);
void AGFEscKey(void);
void AGFLeftKey(void);
void AGFOkKey(void);
void AGFRightKey(void);
void AGFUpKey(void);
void initALARMGOINOFF(void);
void drawAGF(void);
void drawAGFgame(void);

void rsDownKey(void);
void rsEscKey(void);
void rsOkKey(void);
void rsUpKey(void);
void initRadioSelection(void);
void drawRS(void);

void sasDownKey(void);
void sasEscKey(void);
void sasOkKey(void);
void sasUpKey(void);
void initSAS(void);
void drawSAS(void);

void lDownKey(void);
void lEscKey(void);
void lOkKey(void);
void lUpKey(void);
void initL(void);
void drawL(void);

void initLed(void);
void drawLed(void);
void ledEscKey(void);
void ledOkKey(void);
void ledDownKey(void);
void ledUpKey(void);

void initWeather(void);
void drawWeather(void);
void weatherEscKey(void);

int fplus(int x, int y);
int fminus(int x, int y);
int fmultiply(int x, int y);
int fdivide(int x, int y);


//menu definitions
/************************************************************************/
/*
Menu integers are the same as the menu struct array.
This is to make sure the Menus always work well.
*/
/************************************************************************/
#define MENU_RESTARTING -1
#define MENU_SELECT_TIME 0
#define MENU_SET_ALARM 1
#define MENU_SHOW_DATE_TIME 2
#define MENU_SETTINGS 3
#define MENU_NETWORK_INFO 4
#define MENU_SELECT_DATE 5
#define MENU_VOLUME 6
#define MENU_MAC_ADDRESS 7
#define MENU_ALARMGOINOFF 8
#define MENU_RADIO_SELECTION 9
#define MENU_SELECT_ALARM_SOUND 10
#define MENU_LANGUAGE 11
#define MENU_LED 12
#define MENU_WEATHER 13
#define MENU_DEFAULT 14

//Keeps track of the current menu
int currentMenu = MENU_SHOW_DATE_TIME;

//structs

/************************************************************************/
/*
This struct contains the key variable and the pointer to the function that should be called when the key is pressed.
*/
/************************************************************************/
typedef struct {
	unsigned int key;
	void (*action)(void);
} KEY_STRUCT;


/************************************************************************/
/*
This struct defines a menu. The struct contains a key struct array defining what all the keys do when this menu is active.
It contains a pointer to the initialiser of the menu. And a pointer to the "fill strings" or "draw" function of the menu.
*/
/************************************************************************/
typedef struct {
	KEY_STRUCT *keys;
	void (*initialiser)(void);
	void (*fillstrings)(void);
} MENU_STUCT;

/************************************************************************/
/*
This struct contains char arrays for each available language (NE is default?)
*/
/************************************************************************/
// typedef struct {
// 	char *NL;
// 	char *EN;
// 	char *DE;
// } LANGUAGE_STRUCT;
typedef struct {
	const char *translations[3];
} LANGUAGE_STRUCT;


LANGUAGE_STRUCT words[] = {
	{{
		"Keltisch",
		"Celtic",
		"Keltisch"
	}},{{
		"Kerst",
		"Christmas",
		"Weihnachten"
	}},{{
		"Meditatie",
		"Meditation",
		"Meditation"
	}},{{
		"Jazz",
		"Jazz",
		"Jazz"
	}},{{
		"Standaard",
		"Standard",
		"Standard"
	}},{{
		"Radio",
		"Radio",
		"Radio"
	}},{{
		"Wakker worden",
		"Wake up wake up",
		"Wach auf"
	}},{{
		"Volume",
		"Volume",
		"Volumen"
	}},{{
		"Alarmen",
		"Alarms",
		"Alarme"
	}},{{
		"Tijd",
		"Time",
		"Zeit"
	}},{{
		"Datum",
		"Date",
		"Datum"
	}},{{
		"Netwerk",
		"Network",
		"Netzwerk"
	}},{{
		"Taal",
		"Language",
		"Sprache"
	}},{{
		"MAC addres",
		"MAC address",
		"MAC adresse"
	}},{{
		"Info",
		"Info",
		"Info"
	}},{{
		"Geluid Type",
		"Snound Type",
		"Schall Typ"
	}},{{
		"Weer",
		"Weather",
		"Wetter"
	}},{{
		"Ja",
		"Yes",
		"Ja"
	}},{{
		"Nee",
		"No",
		"Nein"
	}},{{
		"Snoozen",
		"Snooze",
		"Schlummern"
	}},{{
		"Fout",
		"Wrong",
		"Falsch"
	}},{{
		"Correct",
		"Correct",
		"richtig"
	}},{{
		NULL,
		NULL,
		NULL
	}}
};


//Select time menu
// MENU_SELECT_TIME
KEY_STRUCT keysSt[] = {
	{
		KEY_UP,
		stUpKey
	},
	{
		KEY_DOWN,
		stDownKey
	},
	{
		KEY_LEFT,
		stLeftKey
	},
	{
		KEY_RIGHT,
		stRightKey
	},
	{
		KEY_OK,
		stOkKey
	},
	{
		KEY_ESC,
		stEscKey
	},
	{
		-1,
		NULL
	}
};
//Set alarm menu
KEY_STRUCT keysSa[] = {
	{
		KEY_UP,
		saUpKey
	},
	{
		KEY_DOWN,
		saDownKey
	},
	{
		KEY_RIGHT,
		saRightKey
	},
	{
		KEY_LEFT,
		saLeftKey
	},
	{
		KEY_ESC,
		saEscKey
	},
	{
		KEY_OK,
		saOkKey
	},
	{
		KEY_01,
		sa1Key
	},
	{
		KEY_02,
		sa2Key
	},
	{
		KEY_03,
		sa3Key
	},
	{
		KEY_04,
		sa4Key
	},
	{
		KEY_05,
		sa5Key
	},
	{
		-1,
		NULL
	}
};
//show date time menu
KEY_STRUCT keysSdt[] = {
	{
		KEY_ALT,
		sdtAltKey
	},
	{
		-1,
		NULL
	}
};
//settings menu
KEY_STRUCT keysSm[] = {
	{
		KEY_UP,
		smUpKey
	},
	{
		KEY_DOWN,
		smDownKey
	},
	{
		KEY_OK,
		smOkKey
	},
	{
		KEY_ESC,
		smEscKey
	},
	{
		-1,
		NULL
	}
};

//settings menu
KEY_STRUCT keysDef[] = {
	{
		KEY_UP,
		defUpKey
	},
	{
		KEY_DOWN,
		defDownKey
	},
	{
		KEY_OK,
		defOkKey
	},
	{
		KEY_ESC,
		defEscKey
	},
	{
		-1,
		NULL
	}
};

//network info menu
KEY_STRUCT keysNi[] = {
	{
		KEY_ESC,
		niEscKey
	},
	{
		-1,
		NULL
	}
};
//Set date menu
KEY_STRUCT keysSd[] = {
	{
		KEY_RIGHT,
		sdRightKey
	},
	{
		KEY_LEFT,
		sdLeftKey
	},
	{
		KEY_UP,
		sdUpKey
	},
	{
		KEY_DOWN,
		sdDownKey
	},
	{
		KEY_ESC,
		sdEscKey
	},
	{
		KEY_OK,
		sdOkKey
	},
	{
		-1,
		NULL
	}
};
//Volume menu
KEY_STRUCT keysV[] = {
	{
		KEY_UP,
		vUpKey
	},
	{
		KEY_DOWN,
		vDownKey
	},
	{
		KEY_ESC,
		vEscKey
	},
	{
		KEY_OK,
		vOkKey
	},
	{
		-1,
		NULL
	}
};
//mac menu
KEY_STRUCT keysMa[] = {
	{
		KEY_LEFT,
		maLefKey
	},
	{
		KEY_RIGHT,
		maRightKey
	},
	{
		KEY_UP,
		maUpKey
	},
	{
		KEY_DOWN,
		maDownKey
	},
	{
		KEY_OK,
		maOkKey
	},
	{
		KEY_ESC,
		maEscKey
	},
	{
		-1,
		NULL
	}
};
//alarmgoinoff menu
KEY_STRUCT keysAGF[] = {
	{
		KEY_01,
		AGF1Key
		},{
		KEY_02,
		AGF2Key
		},{
		KEY_03,
		AGF3Key
		},{
		KEY_04,
		AGF4Key
		},{
		KEY_05,
		AGF5Key
		},{
		KEY_ALT,
		AGFAltKey
		},{
		KEY_DOWN,
		AGFDownKey
		},{
		KEY_ESC,
		AGFSnoozeKey
		},{
		KEY_LEFT,
		AGFLeftKey
		},{
		KEY_OK,
		AGFOkKey
// 		},{
// 		KEY_RIGHT,
// 		AGFSnoozeKey
// 		},{
// 		KEY_UP,
// 		AGFSnoozeKey
		},{
		-1,
		NULL
	}
};
//radio set menu
KEY_STRUCT keysRs[] = {
	{
		KEY_DOWN,
		rsDownKey
		},{
		KEY_UP,
		rsUpKey
		},{
		KEY_OK,
		rsOkKey
		},{
		KEY_ESC,
		rsEscKey
		},{
		-1,
		NULL
	}
};
//led lamp
KEY_STRUCT keysled[] = {
	{
		KEY_DOWN,
		ledDownKey
		},{
		KEY_UP,
		ledUpKey
		},{
		KEY_OK,
		ledOkKey
		},{
		KEY_ESC,
		ledEscKey
		//rsEscKey
		},{
		-1,
		NULL
	}
};
//select_alarm_sound
KEY_STRUCT keysSAS[] = {
	{
		KEY_DOWN,
		sasDownKey
		},{
		KEY_UP,
		sasUpKey
		},{
		KEY_ESC,
		sasEscKey
		},{
		KEY_OK,
		sasOkKey
		},{
		-1,
		NULL
	}
};
//language menu
KEY_STRUCT keysL[] = {
	{
		KEY_DOWN,
		lDownKey
		},{
		KEY_UP,
		lUpKey
		},{
		KEY_ESC,
		lEscKey
		},{
		KEY_OK,
		lOkKey
		},{
		-1,
		NULL
	}
};
//weather menu
KEY_STRUCT keysWeather[] = {
	{
		KEY_ESC,
		weatherEscKey
	},
	{
		-1,
		NULL
	}
};

//Menu array
MENU_STUCT menus[] = {
	{
		keysSt,
		initSelectTime,
		stfillStrings3
	},
	{
		keysSa,
		initSetAlarm,
		saFillStrings
	},
	{
		keysSdt,
		initShowDateTime,
		stdFillStringsPlaceholder
	},
	{
		keysSm,
		initSettingsMenu,
		drawMenuSettings
	},
	{
		keysNi,
		initNetworkInfo,
		drawNi
		
	},
	{
		keysSd,
		initSetDate,
		drawSetDate
	},
	{
		keysV,
		initVolume,
		drawVolume
	},
	{
		keysMa,
		initMacAddress,
		drawMacAddress
	},
	{
		keysAGF,
		initALARMGOINOFF,
		drawAGFgame
	},
	{
		keysRs,
		initRadioSelection,
		drawRS
	},
	{
		keysSAS,
		initSAS,
		drawSAS
	},
	{
		keysL,
		initL,
		drawL
	},
	{
		keysled,
		initLed,
		drawLed
	},
	{
		keysWeather,
		initWeather,
		drawWeather
	},
	{
		keysDef,
		initDefault,
		drawDefault
	},
	{
		NULL,
		NULL
	}
};





//Threads --->
/************************************************************************/
/*
Keeps track of when the Backlight Should be on.
*/
/************************************************************************/
THREAD(lcdWakeUp, arg) //CustomThread
{
	int i;
	for (;;)
	{
		thread_handling_wait(1);
		
		LcdBackLight(LCD_BACKLIGHT_ON);
		for (i = 0; i < 10000; i++)
		{
			thread_handling_wait(1);
			if (resetTimer)
			{
				i = 0;
			}
		}
		LcdBackLight(LCD_BACKLIGHT_OFF);
		blockDoubleThread = 0;
		
		thread_handling_wait(1);
		NutThreadExit();
		while(1){thread_handling_wait(1);}
	}
}

/************************************************************************/
/*
	Updates local clock time syngronized with the internet connection
*/
/************************************************************************/
THREAD(UpdateClockTime, arg) //CustomThread
{
	int tempHour = -1;
	int tempMinutes = -1;
	int tempMonth = -1;
	int tempDay = -1;
	int tempConnection = 0;
	
	while(1)
	{
		thread_handling_wait(1);
		X12RtcGetClock(&tm_datetime);
		
		int timeChange =	tempHour != tm_datetime.tm_hour ||
							tempMinutes != tm_datetime.tm_min ||
							tempMonth != tm_datetime.tm_mon ||
							tempDay != tm_datetime.tm_mday;
		
		int connectionChange = tempConnection != connection;
		int connectionIconWritten = 0;						
	
	
		if(timeChange)
		{
			tempHour = tm_datetime.tm_hour;
			tempMinutes = tm_datetime.tm_min;
			tempMonth = tm_datetime.tm_mon;
			tempDay = tm_datetime.tm_mday;			
			
			char timeString[20];
			char dateString[20];
			
			sprintf(timeString, "     %s", getTimeString());
			sprintf(dateString, "     %s", getDateString());
			
			strcpy(botString, timeString);
			strcpy(topString, dateString);
			
			showDisplay();
			connectionIconWritten = 0;
			
			if(connection)
			{
				showInternetIcon();
				connectionIconWritten = 1;
			}
		}
		
		if(connectionChange)
		{
			tempConnection = connection;
			
			
			if(!connectionIconWritten && connection)
			{
				showInternetIcon();
			}
		}
		
		thread_handling_wait(1000);
		
		if(currentMenu != MENU_SHOW_DATE_TIME)
		{
			
			
			thread_handling_wait(100);
			
			NutThreadExit();
			while(1){thread_handling_wait(1);}
		}
	}
}



/************************************************************************/
/*

*/
/************************************************************************/

//<--- Threads
int* MAC;
void uiInit(void)
{
	
	MAC = (int*) malloc(MAC_ARRAY_SIZE * sizeof(int));
	StorageLoadConfigMac(MAC_ARRAY_SIZE);
}

//standard functions --->
void showInternetIcon(void)
{
	LcdWriteByte(WRITE_COMMAND,0x8F);
	LcdWriteByte(WRITE_DATA,0);
}

/************************************************************************/
/*
Prints the Bot and topString to the display
*/
/************************************************************************/
void showDisplay(void)
{
	LcdLowLevelInit();
	LcdLineOne();
	LcdString(topString);
	LcdLineTwo();
	LcdString(botString);

	if(connection && currentMenu == MENU_SHOW_DATE_TIME)
	{
		showInternetIcon();
	}
}

void updateBottomRowDisplay(void)
{
	LcdLineTwo();
	LcdString(botString);
}

/************************************************************************/
/*
Clears the top and botString
*/
/************************************************************************/
void clearDisplay(void)
{
	int i = 0;
	for(;i < 16;i++){
		topString[i] = ' ';
		botString[i] = ' ';
	}
}

/************************************************************************/
/*
I supposed to be the Method External functions Like threads call to update variables on the display...
*/
/************************************************************************/
void updateVariables(void)
{
	//place code here --->
	
	//<--- place code here
	menus[currentMenu].fillstrings();
}

/************************************************************************/
/*
Is called when a key is pressed and controls the Entire menu structure accordingly
*/
/************************************************************************/
void keyPressEvent(int key)
{	
	if(key == KEY_UNDEFINED)
	{
		blockDoubleThread = 0;	
	}
	
	if(lastPressedKey != key)
	{
		//shortBeeb();
		lastPressedKey = key;
		resetTimer = 1;
		showDisplay();
		int led;
		StorageLoadLed(&led);
		if(2 == led){
			LedControl(LED_ON);
		
			ledTime = tm_datetime.tm_sec + 1;
			if (ledTime > 59)
			{
				ledTime-=59;
			}
		}
		LcdBackLight(LCD_BACKLIGHT_ON);
		lcdTime = tm_datetime.tm_sec + 10;
		if (lcdTime > 59)
		{
			lcdTime-=59;
		}
	}
	else
	{
		resetTimer = 0;
		return;
	}
		
	switch(key)
	{
		case KEY_POWER:
			currentMenu = MENU_RESTARTING;
			strcpy(topString, "Restarting now..");
			LcdBackLight(LCD_BACKLIGHT_ON);
			showDisplay();
		
			thread_handling_wait(900);
		
			UIHandleReset();
		break;
	}
	
	int i = 0;
	for(;menus[currentMenu].keys[i].key != -1;i++){
		if(menus[currentMenu].keys[i].key == key){
			menus[currentMenu].keys[i].action();
			menus[currentMenu].fillstrings();
			return;
		}
	}
	
}


void translate(char *key){
	int idx = 0;
	//char out[17];
	//strcpy(out, *key);
	for(;words[idx].translations[0] != NULL;idx++){
		if(strcmp(key, words[idx].translations[0])==0 || strcmp(key, words[idx].translations[1])==0 || strcmp(key, words[idx].translations[2])==0){
			strcpy(key, words[idx].translations[language]);
			break;
		}
	}
	
}
//<--- standard functions


//menu variables --->
int pos = 0;//position of cursor

int timezone = 0;
// contains data for alarm: type,z,m,d,w,d,v,z
char days[5] =
{
	0b11010100,
	0b11010100,
	0b11010100,
	0b11010100,
	0b11010100
};
int hours[5] = {
	0,
	0,
	0,
	0,
	0
};
int mins[5] = {
	0,
	0,
	0,
	0,
	0
};

unsigned char allarmpos = 0;

char IP[15] = "255.255.255.255";
//00:0C:6E:D2:11:E6

char macPos = 0;//Max11
char shift = 0;
//int mintot = 1;
#define RLENGTH 5
char radios[RLENGTH][17] = {
	"Celtish",
	"Kerst",
	"Meditatie",
	"Jazz",
	" "
};
unsigned char rpos = 0;// < rlength - 2


#define SLENGHT 4
char sounds[4][17] = {
	" Standaard",
	" Radio",
	" Nyan",
	" "
};
unsigned char sPos = 0;

int menPos = 0;

#define LLENGTH 4
char languages[4][17] = {
	"Nederlands",
	"English",
	"Deutsch",
	" "
};

int lPos = 0;


//<--- menu variables

//menu functions --->

//menu: ALARMGOINOFF --->

//variables and extras--->
enum calculations {plus, minus, multiply, divide};
typedef struct {
	enum calculations calculation;
	int (*calfunc)(int x, int y);
	char character;
} FUNC_STRUCT;

FUNC_STRUCT functions[] = {
	{
		plus,
		fplus,
		'+'
	},{
		minus,
		fminus,
		'-'
	},{
		multiply,
		fmultiply,
		'x'
	},{
		divide,
		fdivide,
		':'
	}
};
int fplus(int x, int y){
	return x+y;
}
int fminus(int x, int y){
	return x-y;
}
int fmultiply(int x, int y){
	return x*y;
}
int fdivide(int x, int y){
	return x/y;
}


#define MAXVALUE 19

long val1, val2, val3;
int func1, func2;
int answer;
long inputAnswer = 0;
int okPressed = 0;
int difficulty = 2;//Max4

int values[5];
int valuessize = 0;

int wrong = 0;

int sign = 1;
int power(int x, int y){
	int idx = 0;
	int out = 1;
	for(;idx<y;idx++){
		out = out * x;
	}
	return out;
}
void generate(void)
{
	wrong = 0;
	val1 = rand() % (MAXVALUE+1);
	val2 = rand() % (MAXVALUE+1);
	val3 = rand() % (MAXVALUE+1);
	func1 = (rand() % difficulty);
	func2 = (rand() % difficulty);
	
	if(func1 > func2){
		answer = functions[func1].calfunc(val1, val2);
		answer = functions[func2].calfunc(answer, val3);
	}else{
		answer = functions[func2].calfunc(val2, val3);
		answer = functions[func1].calfunc(val1, answer);
	}
}

long convert(long number,int base){
	if(number == 0 || base==10)
	return number;
	return (number % base) + 10*convert(number / base, base);
}

void updateInput(void){
	int idx = 0;
	inputAnswer = 0;
	for(;idx < valuessize;idx++){
		//printf("%li",inputAnswer);
		//printf(",");
		printf("\n6^%i=",valuessize-(idx+1));
		printf("\n%i", power(6,valuessize-(idx+1)));
		//printf(">");
		inputAnswer+=(values[idx]*power(6,valuessize-(idx+1)));
	}
	//printf("%li",inputAnswer);
}


//<---variables and extras
void initALARMGOINOFF(void){
	currentMenu = MENU_ALARMGOINOFF;
	okPressed = 0;
	generate();
	showDisplay();
}
void AGFSnoozeKey(void){
	snooze(1);
	if(getAlarmType() == ALARMTYPE_RADIO){
		stopStream();
		}else{
		closeBeeping = 1;
		//vsBeepStop();
	}
	
	initShowDateTime();
}

void AGFOkKey(void){
	if(answer == inputAnswer * sign){
		if(getAlarmType() == ALARMTYPE_RADIO){
			stopStream();
		}else{
			closeBeeping = 1;
			//vsBeepStop();
		}
		initShowDateTime();
	}
	else
	{
		wrong++;	
	}
}
void AGFAltKey(void){
	if(valuessize < 5){
		values[valuessize] = 0;
		valuessize++;
	}
}
void AGF1Key(void){
	if(valuessize < 5){
		values[valuessize] = 1;
		valuessize++;
	}
}
void AGF2Key(void){
	if(valuessize < 5){
		values[valuessize] = 2;
		valuessize++;
	}
}
void AGF3Key(void){
	if(valuessize < 5){
		values[valuessize] = 3;
		valuessize++;
	}
}
void AGF4Key(void){
	if(valuessize < 5){
		values[valuessize] = 4;
		valuessize++;
	}
}
void AGF5Key(void){
	if(valuessize < 5){
		values[valuessize] = 5;
		valuessize++;
	}
}
void AGFLeftKey(void){
	if(valuessize > 0){
		valuessize--;
	}
}
void AGFDownKey(void){
	sign = sign * (-1);
}

void drawAGFgame(void){
	clearDisplay();
	updateInput();
	//printf("%li %i",inputAnswer, convert(inputAnswer,6));
	
	sprintf(topString, "%li%c%li%c%li base6", convert(val1,6), functions[func1].character, convert(val2,6), functions[func2].character, convert(val3,6));
	sprintf(botString, "=%li         ", convert(inputAnswer*sign ,6));
	
	int idx = 0;
	
	for(;idx<15 && idx < wrong;idx++){
		botString[idx+7] = '!';
		botString[idx+8] = '\0';
	}
}

void drawAGF(void){
	clearDisplay();
	char text[17] = "Wake up wake up";
	translate(text);
	strcpy(topString, text);
	strcpy(botString, text);
	showDisplay();
}

//<--- ALARMGOINOFF
//menu: Language --->
void initL(void){
	currentMenu = MENU_LANGUAGE;
}

void lDownKey(void){
	if(lPos < LLENGTH-2){
		lPos++;
	}
}
void lUpKey(void){
	if(lPos > 0){
		lPos--;
	}
}
void lOkKey(void){
	language = lPos;
	initSettingsMenu();
}
void lEscKey(void){
	initSettingsMenu();
}

void drawL(void){
	clearDisplay();
	sprintf(topString, " %s",languages[lPos]);
	sprintf(botString, " %s",languages[lPos+1]);
	topString[0] = '>';
	showDisplay();
}
//<---Language

//menu: Select alarm sound --->
void initSAS(void){
	currentMenu = MENU_SELECT_ALARM_SOUND;
	
}
void sasDownKey(void){
	if(sPos < SLENGHT-2){
		sPos++;
	}
}
void sasUpKey(void){
	if(sPos > 0){
		sPos--;
	}
}
void sasOkKey(void){
	setAlarmType(sPos);
	initSettingsMenu();
}
void sasEscKey(void){
	initSettingsMenu();
}
void drawSAS(void){
	clearDisplay();
	translate( sounds[sPos]);
	translate( sounds[sPos+1]);
	sprintf(topString, " %s",sounds[sPos]);
	sprintf(botString, " %s",sounds[sPos+1]);
	topString[0] = '>';
	showDisplay();
}
//<--- Select alarm sound

//menu: Radio selection --->
void initRadioSelection(void){
	currentMenu = MENU_RADIO_SELECTION;
	StorageLoadRadioChar(&rpos);
}

void rsDownKey(void){
	if(rpos < RLENGTH-2){
		rpos++;
	}
}
void rsUpKey(void){
	if(rpos > 0){
		rpos--;
	}
}
void rsOkKey(void){
	StorageSaveRadioChar(&rpos);
	initSettingsMenu();
}
void rsEscKey(void){
	initSettingsMenu();
}

void drawRS(void){
	clearDisplay();
	translate(radios[rpos]);
	translate(radios[rpos+1]);
	sprintf(topString, " %s",radios[rpos]);
	sprintf(botString, " %s",radios[rpos+1]);
	topString[0] = '>';
	showDisplay();
}
//<--- Radio selection

//menu: ALARMGOINOFF --->
// void initALARMGOINOFF(void){
// 	currentMenu = MENU_ALARMGOINOFF;
// 	showDisplay();
// }
// void AGFSnoozeKey(void){
// 	int alarmOn = 2;
// 	StorageSaveAlarmOn(&alarmOn);
// 	snooze(1);
// 	if(getAlarmType() == ALARMTYPE_RADIO){
// 		stopStream();
// 	}else{
// 		closeBeeping = 1;
// 		//vsBeepStop();
// 	}
// 	
// 	initShowDateTime();
// }
// 
// void AGFOkKey(void){
// 	int alarmOn = 2;
// 	StorageSaveAlarmOn(&alarmOn);
// 	if(getAlarmType() == ALARMTYPE_RADIO){
// 		stopStream();
// 	}else{
// 		closeBeeping = 1;
// 		//vsBeepStop();
// 	}
// 	initShowDateTime();
// }

//menu: Mac address --->
void initMacAddress(void)
{
	currentMenu = MENU_MAC_ADDRESS;
	
	StorageLoadConfigMac(MAC_ARRAY_SIZE);
	
	macPos = 0;
	
	showDisplay();
}
void maRightKey(void){
	if(macPos < 11){
		macPos++;
	}
	if(macPos > 9){
		shift = 1;
	}
}
void maLefKey(void){
	if(macPos > 0){
		macPos--;
	}
	if(macPos < 2){
		shift = 0;
	}
}
void maUpKey(void){
	if(macPos%2==0){
		MAC[macPos/2]+=0x10;
		if(MAC[macPos/2] > 0xFF){
			MAC[macPos/2] = 0;
		}
		}else{
		MAC[(macPos-1)/2]++;
		if(MAC[(macPos-1)/2] > 0xFF){
			MAC[(macPos-1)/2] = 0;
		}
	}
	
}
void maDownKey(void){
	if(macPos%2==0){
		MAC[macPos/2]-=0x10;
		if(MAC[macPos/2] < 0){
			MAC[macPos/2] = 0xFF;
		}
		}else{
		MAC[(macPos-1)/2]--;
		if(MAC[(macPos-1)/2] < 0){
			MAC[(macPos-1)/2] = 0xFF;
		}
	}
}

void maEscKey(void)
{
	initSettingsMenu();
}

void maOkKey(void)
{
	StorageSaveConfigMac(MAC_ARRAY_SIZE);
	maEscKey();
}

void drawMacAddress(void)
{
	clearDisplay();
	if(shift == 0){
		sprintf(topString, "%02X:%02X:%02X:%02X:%02X>", MAC[0], MAC[1], MAC[2], MAC[3], MAC[4] );
		botString[((macPos*3)/2)] = '^';
	}
	else if(shift == 1){
		sprintf(topString, "<%02X:%02X:%02X:%02X:%02X", MAC[1], MAC[2], MAC[3], MAC[4], MAC[5]);
		botString[((macPos*3)/2)-2] = '^';
	}
	showDisplay();
}
//<--- Mac address
//menu: Volume --->
void initVolume(void)
{
	StorageLoadConfigV(&volume);
	
	int i;
	for(i = 0; 11 > i; i++){
		if(11 == i){
			pos = 0;
			volume = 100;
			break;
		}else
		if(volume == volumes[i]){
			pos = i;
			break;
		}
		
	}
	currentMenu = MENU_VOLUME;

	
}
void vEscKey(void)
{
	pos = 0;
	initSettingsMenu();
}
void vOkKey(void)
{
	pos = 0;
	StorageSaveConfigV(&volume);
	vEscKey();
}


void vUpKey(void)
{
	if(10 != pos){
	volume = volumes[++pos];

	VsSetVolume((volume - 250)* -1, (volume - 250)* -1);
	VsBeepSelfMade(50);
	//VsBeepSelfMade(50);
	}
	}
void vDownKey(void)
{
	if(0 != pos){
	volume = volumes[--pos];
	
	VsSetVolume((volume - 250)* -1, (volume - 250)* -1);
	VsBeepSelfMade(50);
	//VsBeepSelfMade(50);
	}
}
void drawVolume(void)
{
	clearDisplay();
	char text[17] = "Volume";
	translate(text);
	sprintf(topString, "%s:", text);
	
	int i;
	for(i = 0;  pos > i;i++){
		botString[i+6] = 1;
	}
	showDisplay();
	
}
//<---Volume

//menu: network info --->
void initNetworkInfo(void){
	currentMenu = MENU_NETWORK_INFO;
}
void niEscKey(void){
	initSettingsMenu();
}
void drawNi(void){
	strcpy(topString, IP);
	showDisplay();
}
//<--- network info

//Menu: set alarm --->
void initSetAlarm(void)
{
	unsigned char i;
	char loadedHour;
	char loadedMin;
	char loadedFlags;
	
	currentMenu = MENU_SET_ALARM;
	
	for(i = 0 ; i < 5 ; i++)
	{
		loadAlarm(&loadedHour,&loadedMin,&loadedFlags, i);
		hours[i] = loadedHour;
		mins[i] = loadedMin;
		days[i] = loadedFlags;
	}
	hour = hours[0];
	min = mins[0];
}
void saLeftKey(void){
	if(pos > 0){
		pos--;
	}
}
void saRightKey(void){
	if(pos < 9){
		pos++;
	}
}
void saUpKey(void){
	if(pos == 0){
		//mintot += 60;
		hourPlus();
	}
	else if(pos == 1){
		//mintot += 1;
		minPlus();
	}
	else
	{
		if(pos == 9){
			days[allarmpos]^=(1 << (7));
		}else
		days[allarmpos]^=(1 << (8-(pos)));
	}
}
void saDownKey(void){
	if(pos == 0){
		//mintot -= 60;
		hourMinus();
	}
	else if(pos == 1){
		//mintot -= 1;
		minMinus();
	}
	else
	{
		if(pos == 9){
			days[allarmpos]^=(1 << (7));
		}else
		days[allarmpos]^=(1 << (8-(pos)));
	}
}
void sa1Key(void){
	hours[allarmpos] = hour;
	mins[allarmpos] = min;
	allarmpos = 0;
	hour = hours[allarmpos];
	min = mins[allarmpos];
}
void sa2Key(void){
	hours[allarmpos] = hour;
	mins[allarmpos] = min;
	allarmpos = 1;
	hour = hours[allarmpos];
	min = mins[allarmpos];
}
void sa3Key(void){
	hours[allarmpos] = hour;
	mins[allarmpos] = min;
	allarmpos = 2;
	hour = hours[allarmpos];
	min = mins[allarmpos];
}
void sa4Key(void){
	hours[allarmpos] = hour;
	mins[allarmpos] = min;
	allarmpos = 3;
	hour = hours[allarmpos];
	min = mins[allarmpos];
}
void sa5Key(void){
	hours[allarmpos] = hour;
	mins[allarmpos] = min;
	allarmpos = 4;
	hour = hours[allarmpos];
	min = mins[allarmpos];
}
void saEscKey(void){
	initSettingsMenu();
	allarmpos = 0;
}

void saOkKey(void)
{
	saveAlarm(&days[allarmpos],&min,&hour,allarmpos);
	saEscKey();
	allarmpos = 0;
}

void saFillStrings(void)
{
	clearDisplay();

	if(pos<9)
	{
		topString[0] = ((hour - hour%10)/10)+48;
		topString[1] = (hour%10)+48;
		topString[2] = ':';
		topString[3] = ((min - min%10)/10)+48;
		topString[4] = (min%10)+48;
		topString[5] = ' ';
		
		if((days[allarmpos] >> 6) & 1)topString[6] = 'Z';
		else topString[6] = '_';
		if((days[allarmpos] >> 5) & 1)topString[7] = 'M';
		else topString[7] = '_';
		if((days[allarmpos] >> 4) & 1)topString[8] = 'D';
		else topString[8] = '_';
		if((days[allarmpos] >> 3) & 1)topString[9] = 'W';
		else topString[9] = '_';
		if((days[allarmpos] >> 2) & 1)topString[10] = 'D';
		else topString[10] = '_';
		if((days[allarmpos] >> 1) & 1)topString[11] = 'V';
		else topString[11] = '_';
		if((days[allarmpos] >> 0) & 1)topString[12] = 'Z';
		else topString[12] = '_';
		
		topString[15] = '>';
	}else{
			
		char answer[5] = "Nee";
		char text[17] = "Snooze";
		if((days[allarmpos] >> 7) & 1){
			 sprintf(answer, "Ja");
		}
		translate(answer);
		translate(text);
		sprintf(topString,"%s:%s", text, answer);
	}
	
	if(pos < 2)
	botString[pos*3+1] = '^';
	else if(pos < 9)
	botString[pos+4] = '^';
	
	botString[15] = allarmpos+48+1;
	showDisplay();
}
//<--- set alarm

//Menu: set menu --->
void initSettingsMenu(void)
{
	currentMenu = MENU_SETTINGS;
}

void smUpKey(void){
	if(menPos != 0){
		menPos--;
	}
}
void smDownKey(void){
	if(menuItems-1 != menPos){
		menPos++;
	}
}
void smEscKey(void){
	initShowDateTime();
}
void smOkKey(void){
	switch(menPos){
		case(Alarmen):
		initSetAlarm();
		return;
		break;
		case(tijd):
		initSelectTime();
		return;
		break;
		case(datum):
		initSetDate();
		return;
		break;
		case(taal):
		initL();
		break;
		case(radio):
		initRadioSelection();
		break;
		case(volumeSettings):
		initVolume();
		return;
		break;
		case(MAC_addres):
		initMacAddress();
		break;
		case(sas):
		initSAS();
		break;
		case(led):
		initLed();
		break;
		case(weather):
		initWeather();
		break;
		case(reset):
		initDefault();
		return;
		break;
	}
}

	/* 
     *  main menu for options you can select
     */
void drawMenuItem(int top, int sel){
	char woorden[13][17] = {{"Alarmen"},{"Tijd"},{"Datum"},{"Radio"},{"Taal"},{"Volume"},{"MAC addres"},{"Geluid Type"},{"Led"},{"Weer"},{"Standaard"}};
	
	translate(woorden[sel]);
	translate(woorden[sel+1]);
	
	if(sel%2){
		sprintf(topString, "   %s", woorden[sel]);
		sprintf(botString, "   %s", woorden[sel+1]);	
	}else{
		sprintf(topString, "   %s", woorden[sel]);
		sprintf(botString, "   %s", woorden[sel+1]);
	}
	
	/*
void drawMenuItem(int top, enum selected sel){
	char woorden[11][15] = {{"Alarmen"},{"Tijd"},{"Datum"},{"Netwerk"},{"Radio"},{"Taal"},{"Volume"},{"MAC adress"},{"Info"},{"Select Sound"},{"Led"}};
	//char woorden[10][15] = {{"Alarmen"},{"Tijd"},{"Datum"},{"Netwerk"},{"Radio"},{"Taal"},{"Volume"},{"MAC addres"},{"Info"},{"Geluid Type"}};
	int i;
	
	switch(sel){
		case(Alarmen):
		for(i = 0; i<7;i++){
			if(top)
			topString[i+5] = woorden[0][i];
			else
			botString[i+5] = woorden[0][i];
		}
		break;
		case(tijd):
		for(i = 0; i<4;i++){
			if(top)
			topString[i+6] = woorden[1][i];
			else
			botString[i+6] = woorden[1][i];
		}
		break;
		case(datum):
		for(i = 0; i<5;i++){
			if(top)
			topString[i+6] = woorden[2][i];
			else
			botString[i+6] = woorden[2][i];
		}
		break;
		case(netwerk):
		for(i = 0; i<7;i++){
			if(top)
			topString[i+5] = woorden[3][i];
			else
			botString[i+5] = woorden[3][i];
		}
		break;
		case(radio):
		for(i = 0; i<5;i++){
			if(top)
			topString[i+6] = woorden[4][i];
			else
			botString[i+6] = woorden[4][i];
		}
		break;
		case(taal):
		for(i = 0; i<4;i++){
			if(top)
			topString[i+6] = woorden[5][i];
			else
			botString[i+6] = woorden[5][i];
		}
		break;
		case(volumeSettings):
		for(i = 0; i<6;i++){
			if(top)
			topString[i+5] = woorden[6][i];
			else
			botString[i+5] = woorden[6][i];
		}
		break;
		case(MAC_addres):
		for(i = 0; i<10;i++){
			if(top)
			topString[i+4] = woorden[7][i];
			else
			botString[i+4] = woorden[7][i];
		}
		break;
		case(info):
		for(i = 0; i<4;i++){
			if(top)
			topString[i+6] = woorden[8][i];
			else
			botString[i+6] = woorden[8][i];
		}
		break;
		case(sas):
		for(i = 0; i<11;i++){
			if(top)
			topString[i+2] = woorden[9][i];
			else
			botString[i+2] = woorden[9][i];
		}
		break;
		case(led):
		for(i = 0; i<3;i++){
			if(top)
			topString[i+6] = woorden[10][i];
			else
			botString[i+6] = woorden[10][i];
		}
		break;
	}
	*/
}
void drawMenuSettings(void){
	clearDisplay();
	if(menPos<menuItems){
		
		if(menPos == menuItems - 1){
			
			drawMenuItem(0,menPos-1);
			botString[0] = '>';
			
			//drawMenuItem(1,menuPos - 1);
			//drawMenuItem(0,menuPos);
		}
		else{
			drawMenuItem(0,menPos);
			topString[0] = '>';
			
			//drawMenuItem(1,menuPos);
			//drawMenuItem(0,menuPos + 1);
		}
	}
}
//<--- set menu

//Menu: show date & time --->
void initShowDateTime(void)
{
	currentMenu = MENU_SHOW_DATE_TIME;
	
	dateTimeInit();

	clearDisplay();
	
	NutThreadCreate("time", UpdateClockTime, NULL, 512);
}

void sdtAltKey(void){
	initSettingsMenu();
}

void stdFillStringsPlaceholder(void)
{
	return;
}
int UIHandleReset(void)
{
	WatchDogEnable();
	WatchDogStart(30);
	
	for (;;) 
	{
		thread_handling_wait(1);
	}
	
	return 0;
}
//<-- show date & time

//Menu: set SET DATE --->

void initSetDate(void)
{
	currentMenu = MENU_SELECT_DATE;
	
	day = tm_datetime.tm_mday;
	month = tm_datetime.tm_mon + 1;
	year = 2019;
}

void dayCheck(){
	if(year % 4 == 0 && month == 2 && day > 29){
		day = daysMonth[1] + 1;
	}else
	if(day > daysMonth[month - 1]){
		if(year % 4 == 0 && month ==2){
			day = daysMonth[month - 1] + 1;
			}else{
			day = daysMonth[month - 1];
		}
	}
	
}
void sdDownKey(void){
	if(pos == 0){
		if(day == 1){
			if(year % 4 == 0 && month == 2){
				day = daysMonth[month-1] +1;
				}else{
				day = daysMonth[month- 1];
			}
			}else{
			day--;
		}
	}else
	if(pos == 1){
		if(month == 1){
			month = 12;
		}else
		month--;
		dayCheck();
	}else
	if(pos == 2){
		if(year != 1970){
			year--;
		}
		dayCheck();
	}
}
void sdUpKey(void){
	if(pos == 0){
		if(day == daysMonth[month-1]){
			if(year % 4 == 0 && month == 2 && day == 28){
				day++;
			}else
			{
				day = 1;
			}
		}else
		if(year % 4 == 0 && month == 2 && day > 28){
			day = 1;
			}else{
			day++;
		}
		
	}else
	if(pos == 1){
		if(month == 12){
			month = 1;
			}else{
			month++;
			dayCheck();
		}
	}else
	if(pos == 2){
		if(year != 2040){
			year++;
		}
		dayCheck();
	}
}
void sdLeftKey(void){
	if(pos != 0)
	pos--;
}
void sdRightKey(void){
	if(pos != 2)
	pos++;
}
void sdEscKey(void){
	initSettingsMenu();
	
	pos = 0;
}

void sdOkKey(void)
{
	setDate(day, month-1);
	setWDay(dayofweek(day, month, year));
	
	sdEscKey();
}

void drawSetDate(void){
	clearDisplay();
	char text[10];
	
	sprintf(text,"%02d/%02d/%04d",day,month,year);
	int i;
	for(i = 0; i < 10; ++i){
		topString[i+2] = text[i];
	}
	
	if(pos == 0){
		botString[3] = '^';
	}else
	if(pos == 1){
		botString[6] = '^';
	}
	else
	if(pos == 2){
		botString[11] = '^';
	}
	showDisplay();

}
//<--- set menu

//Menu: select time --->
void initSelectTime(void)
{
	currentMenu = MENU_SELECT_TIME;
	hour = tm_datetime.tm_hour;
	min = tm_datetime.tm_min;
	timezone = (int) (getTimeZone()*2.0);
	pos = 0;
}
//void setVolume(void)
//{
//
//volumeIsSet = 0;
//
//
//
//
//
//VsSetVolume(volume,volume);
//
//}
void stLeftKey(void){
	if(pos > 0){
		pos--;
	}
}
void stRightKey(void){
	if(pos < 2){
		pos++;
	}
}
void stUpKey(void){
	if(pos == 0){
		//mintot += 60;
		hourPlus();
	}
	else if(pos == 1){
		//mintot += 1;
		minPlus();
	}
	else if(pos == 2){
		timezonePlus();
	}
}
void stDownKey(void){
	if(pos == 0){
		//mintot -= 60;
		hourMinus();
	}
	else if(pos == 1){
		//mintot -= 1;
		minMinus();
	}
	else if(pos == 2){
		timezoneMinus();
	}
}
void stOkKey(void){
	setTime(hour, min);
	setTimeZone(timezone);
	initShowDateTime();
	showDisplay();
}
void stEscKey(void){
	initSettingsMenu();
	showDisplay();
}
void stfillStrings3(void)
{
	clearDisplay();
	if(timezone%2 == 0)
	{
		sprintf(topString, "%02d:%02d UTC%+i", hour, min, (timezone/2));
		}else{
		if(timezone >= 0)
		{
			sprintf(topString, "%02d:%02d UTC%+i.%i", hour, min, ((timezone-1)/2), 5);
			}else if(timezone != -1){
			sprintf(topString, "%02d:%02d UTC%+i.%i", hour, min, ((timezone+1)/2), 5);
			}else{
			sprintf(topString, "%02d:%02d UTC-%i.%i", hour, min, ((timezone+1)/2), 5);
		}
	}
	botString[pos*3+1] = '^';
	showDisplay();
}
//<--- select time
//<--- menu functions

//Util stuff:
/************************************************************************/
/*
The following six functions make sure the Time variables in the settings menu are raised and lowered appropriately
*/
/************************************************************************/
void minPlus(void)
{
	if(min >= 59){
		hourPlus();
		min = 0;
		}else{
		min++;
	}
}
void minMinus(void)
{
	if(min <= 0){
		hourMinus();
		min = 59;
		}else{
		min--;
	}
}
void hourPlus(void)
{
	if(hour >= 23){
		hour = 0;
		}else{
		hour++;
	}
}
void hourMinus(void)
{
	if(hour <= 0){
		hour = 23;
		}else{
		hour--;
	}
}
void timezonePlus(void)
{
	if(timezone >= (13*2))
	{
		timezone = (-10*2);
	}
	else timezone++;
}
void timezoneMinus(void)
{
	if(timezone <= (-10*2))
	{
		timezone = (13*2);
	}
	else timezone--;
}

void checkLcdBackLight(void)
{
	X12RtcGetClock(&tm_datetime);
	
	if (lcdTime != -1 && lcdTime == tm_datetime.tm_sec)
	{
		lcdTime = -1;
		LcdBackLight(LCD_BACKLIGHT_OFF);
	}
	int led;
	StorageLoadLed(&led);
	if (ledTime != -1 && ledTime == tm_datetime.tm_sec && 2 == led)
	{
		ledTime = -1;
		LedControl(LED_OFF);
	}
}


// led display -->

void initLed(void){
	currentMenu = MENU_LED;
	StorageLoadLed(&pos);
	if(!(pos >= 0 && pos <= 2))
		pos = 0;

}

void drawLed(void){
	clearDisplay();
	switch(pos){
	case 0:
		topString[0] = '>';
		topString[6] = 'O';
		topString[7] = 'n';
	
		botString[6]= 'O';
		botString[7]= 'f';
		botString[8]= 'f';
		break;
	case 1:
		topString[0] = '>';
		
		topString[6]= 'O';
		topString[7]= 'f';
		topString[8]= 'f';
		
		botString[4]= 'T';
		botString[5]= 'o';
		botString[6]= 'g';
		botString[7]= 'g';
		botString[8]= 'e';
		botString[9]= 'l';
		break;
	
	case 2:
		botString[0] = '>';
	
		topString[6]= 'O';
		topString[7]= 'f';
		topString[8]= 'f';
		
		botString[4]= 'T';
		botString[5]= 'o';
		botString[6]= 'g';
		botString[7]= 'g';
		botString[8]= 'e';
		botString[9]= 'l';
		break;
		
}
showDisplay();
}

void ledEscKey(void){
	pos = 0;
	initSettingsMenu();
}

void ledOkKey(void){
	StorageSaveLed(&pos);
	if(0 == pos){
		LedControl(LED_ON);
	}
	else{
		LedControl(LED_OFF);
	}
	pos = 0;
	
	initSettingsMenu();
}

void ledDownKey(void){
	if(2 != pos)
		pos++;
}

void ledUpKey(void){
	if(0 != pos)
		pos--;
}

//<-- led display

// weather display -->

THREAD(text_thread, arg) //CustomThread
{
	int goUp = 1;
	int i;
	int j = 0;
	char temp[17];
	
	while(1)
	{
		for (i = 0; i < 16; i++)
		{
			if('\0' == _weather.expectation[i+j])
			{
				break;
			}
			
			temp[i] = _weather.expectation[i+j];
		}
		temp[16] = '\0';
		
		if(MENU_WEATHER == currentMenu)
		{
			strcpy(botString, temp);
			updateBottomRowDisplay();
		}
		
		int oldJ = j;
		
		if(1 == goUp)
		{
			j++;
		}
		else
		{
			j--;
		}
		
		if(_weather.expectation[16 + oldJ] != '\0' && _weather.expectation[16 + oldJ + 1] == '\0')
		{
			goUp = 0;
		}
		
		if(j <= 0)
		{
			goUp = 1;
		}
		
		if(MENU_WEATHER != currentMenu)
		{
			menuItems = 11;
			NutThreadExit();
			while(1){thread_handling_wait(1);}
		}
		
		thread_handling_wait(200);
	}
}

void initWeather(void)
{
	currentMenu = MENU_WEATHER;
	menuItems = 11;
	weather_init();
}

void drawWeather(void)
{
	clearDisplay();
	
	switch(strlen(_weather.temperature)) //sets how many spaces must be between "Breda" and the temperature to allign the temperature correctly
	{
		case 5:
			sprintf(topString, "%s    %s C", _weather.location, _weather.temperature);
		break;
		case 4:
			sprintf(topString, "%s     %s C", _weather.location, _weather.temperature);
		break;
		case 3:
			sprintf(topString, "%s      %s C", _weather.location, _weather.temperature);
		break;
		default:
			sprintf(topString, "%s   %s C", _weather.location, _weather.temperature);
		break;
	}
	
	strcpy(botString, _weather.expectation);
	
	if(strlen(_weather.expectation) > 16)
	{
		NutThreadCreate("text", text_thread, NULL, 2048);
	}
	else
	{
		showDisplay();
	}
}

void weatherEscKey(void)
{
	initSettingsMenu();
	showDisplay();
}

//<-- weather display


// default menu -->

void initDefault(void){
	currentMenu = MENU_DEFAULT;
	pos = 0;
}

void drawDefault(void){
	clearDisplay();
	topString[0] = 'D';
	topString[1] = 'E';
	topString[2] = 'F';
	topString[3] = 'A';
	topString[4] = 'U';
	topString[5] = 'L';
	topString[6] = 'T';
	topString[7] = ':';
	
 topString[9] = 'N';
 topString[10] = 'O';
 
 botString[9] = 'Y';
 botString[10] = 'E';
 botString[11] = 'S';
 
 if(0 == pos){
	 topString[8] = '>';
}
else{
	 botString[8] = '>';
}
}

void defEscKey(void){
	pos = 0;
	initSettingsMenu();
}

void defOkKey(void){
	if(0 == pos){
		initSettingsMenu();
	}
	else{
		char day = 0;
		char hour = 1;
		char min = 1;
		StorageSaveAlarm1(&day,&min,&hour);
		StorageSaveAlarm2(&day,&min,&hour);
		StorageSaveAlarm3(&day,&min,&hour);
		StorageSaveAlarm4(&day,&min,&hour);
		StorageSaveAlarm5(&day,&min,&hour);
		StorageSaveAlarm6(&day,&min,&hour);
		
		int value = 1;
		StorageSaveLed(&value);
		
		value = 180;

		StorageSaveConfigV(&value);
		
		value = 2;
		StorageSaveConfigT(&value);
		
		initShowDateTime();
	}
	pos = 0;
}

void defDownKey(void){
	if(1 != pos)
	pos++;
}

void defUpKey(void){
	if(0 != pos)
	pos--;
}




// <-- default menu

