
/*
 * threadHandling.c
 *
 * Created: 12/03/2019 10:56:35
 *  Author: Carlos Cadel
 */ 

#include <sys/timer.h>

/**
 * \brief used as wait function in threads
 * 
 * \param ms milliseconds to wait
 * 
 * \return void
 */
void thread_handling_wait(int ms)
{
	int i;
	for(i = 0; i < ms; i++)
	{
		NutSleep(1);
	}
}
