/************************
 *Storage abstraction class. Allows quick volatile storage of           
 *any short amount of information.                                                                 
 *                                                                    
 ************************/
#include "storage.h"
#include "log.h"
#include <dev/board.h>
#include <dev/nvmem.h>
#include <time.h>
#include <stdio.h>

int* MAC;

	/* 
     *  save and load global settings
     */
void StorageSaveConfigAll(int* T,int* L,int* V){
	StorageSaveConfigT(T);
	StorageSaveConfigV(V);
	StorageSaveConfigL(L);
}
void StorageLoadConfigAll(int* T, int* L, int* V){
	StorageLoadConfigT(T);
	StorageLoadConfigV(V);
	StorageLoadConfigL(L);
}

void StorageSaveLed(int* led){
	NutNvMemSave(443, led, sizeof(int));
}

void StorageLoadLed(int* led){
	NutNvMemLoad(443, led, sizeof(int));
}

	/* 
     *  save and load methodes of each individual alarm 
	 * each methode saves/loads the data on an specific memory location
     */
void StorageSaveAlarm1(char* day, char* min, char* hour){
	NutNvMemSave(402, day, sizeof(char));
	NutNvMemSave(405, min, sizeof(char));
	NutNvMemSave(407, hour, sizeof(char));
}

void StorageLoadAlarm1(char* day, char* min, char* hour){
	NutNvMemLoad(402, day, sizeof(char));
	NutNvMemLoad(405, min, sizeof(char));
	NutNvMemLoad(407, hour, sizeof(char));
}

void StorageSaveAlarm2(char* day, char* min, char* hour){
	NutNvMemSave(409, day, sizeof(char));
	NutNvMemSave(411, min, sizeof(char));
	NutNvMemSave(413, hour, sizeof(char));
}

void StorageLoadAlarm2(char* day, char* min, char* hour){
	NutNvMemLoad(409, day, sizeof(char));
	NutNvMemLoad(411, min, sizeof(char));
	NutNvMemLoad(413, hour, sizeof(char));
}

void StorageSaveAlarm3(char* day, char* min, char* hour){
	NutNvMemSave(415, day, sizeof(char));
	NutNvMemSave(417, min, sizeof(char));
	NutNvMemSave(419, hour, sizeof(char));
}

void StorageLoadAlarm3(char* day, char* min, char* hour){
	NutNvMemLoad(415, day, sizeof(char));
	NutNvMemLoad(417, min, sizeof(char));
	NutNvMemLoad(419, hour, sizeof(char));
}

void StorageSaveAlarm4(char* day, char* min, char* hour){
	NutNvMemSave(421, day, sizeof(char));
	NutNvMemSave(423, min, sizeof(char));
	NutNvMemSave(425, hour, sizeof(char));
}

void StorageLoadAlarm4(char* day, char* min, char* hour){
	NutNvMemLoad(421, day, sizeof(char));
	NutNvMemLoad(423, min, sizeof(char));
	NutNvMemLoad(425, hour, sizeof(char));
}

void StorageSaveAlarm5(char* day, char* min, char* hour){
	NutNvMemSave(427, day, sizeof(char));
	NutNvMemSave(429, min, sizeof(char));
	NutNvMemSave(431, hour, sizeof(char));
}

void StorageLoadAlarm5(char* day, char* min, char* hour){
	NutNvMemLoad(427, day, sizeof(char));
	NutNvMemLoad(429, min, sizeof(char));
	NutNvMemLoad(431, hour, sizeof(char));
}

void StorageSaveAlarm6(char* day, char* min, char* hour){
	NutNvMemSave(433, day, sizeof(char));
	NutNvMemSave(435, min, sizeof(char));
	NutNvMemSave(437, hour, sizeof(char));
}

void StorageLoadAlarm6(char* day, char* min, char* hour){
	NutNvMemLoad(433, day, sizeof(char));
	NutNvMemLoad(435, min, sizeof(char));
	NutNvMemLoad(437, hour, sizeof(char));
}

void StorageSaveRadioChar(unsigned char* setting){
	NutNvMemSave(439, setting, sizeof(char));
}

void StorageLoadRadioChar(unsigned char* setting){
	NutNvMemLoad(439, setting, sizeof(char));
}

void StorageSaveConfigL(int* setting){
	NutNvMemSave(513, setting, sizeof(int));
}

void StorageLoadConfigL(int* setting){
	NutNvMemLoad(513, setting, sizeof(int));
}

void StorageSaveConfigT(int* setting){
	NutNvMemSave(500, setting, sizeof(setting));
}

void StorageLoadConfigT(int* setting){
	NutNvMemLoad(500, setting, sizeof(setting));
}

void StorageSaveConfigV(int* setting){
	NutNvMemSave(510, setting, sizeof(setting));
}

void StorageLoadConfigV(int* setting){
	NutNvMemLoad(510, setting, sizeof(setting));
}

void StorageSaveAlarmOn(int* setting){
	NutNvMemSave(445, setting, sizeof(int));
}

void StorageLoadAlarmOn(int* setting){
	NutNvMemLoad(445, setting, sizeof(int));
}



	/* 
     *  save/load methodes of the Mac address
     */
void StorageSaveConfigMac(int size)
{
	int i;
	for(i = 0; i < size; i++)
	{
		NutNvMemSave(256 + (2*i), &MAC[i], sizeof (MAC[i]));
	}
}

void StorageLoadConfigMac(int size)
{
	int i;
	for(i = 0; i < size; i++)
	{
		NutNvMemLoad(256 + (2*i), &MAC[i], sizeof (MAC[i]));
	}
	
	int j;
	for (j = 0; j < size; j++)
	{
		if(MAC[j] > 0xFF || MAC[j] < 0x00)
		{
			
			
			MAC[0] = 0x11;
			MAC[1] = 0x11;
			MAC[2] = 0x11;
			MAC[3] = 0x11;
			MAC[4] = 0x11;
			MAC[5] = 0x11;
			
			StorageSaveConfigMac(size);			
			return;
		}
	}
}
