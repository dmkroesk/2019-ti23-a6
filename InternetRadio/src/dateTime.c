/*
* dateTime.c
*
* Created: 19-Feb-19 13:35:08
*  Author: svber & Carlos Cadel
*/

#define LOG_MODULE  LOG_MAIN_MODULE
#define CHAR_ARRAY_SIZE_TIME 8

#include <stdio.h>
#include <string.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "dateTime.h"
#include "storage.h"

#include <time.h>
#include "rtc.h"

tm tm_datetime;
int utcPlusTimeZone;

/**
 * \brief init method for dateTime.c, used when time should also be reset
 * 
 * \param 
 * 
 * \return void
 */
void newDateTimeInit(void)
{
	tm_datetime = (tm) {0, 0, 0, 0, 0, 0, 0, 0, 0};
	utcPlusTimeZone = 0.0;
	StorageSaveConfigT(&utcPlusTimeZone);
	
	X12RtcSetClock(&tm_datetime);
}

/**
 * \brief init method for dateTime.c
 * 
 * \param 
 * 
 * \return void
 */
void dateTimeInit(void)
{
	tm_datetime = (tm) {0, 0, 0, 0, 0, 0, 0, 0, 0};
	StorageLoadConfigT(&utcPlusTimeZone);
	
	X12RtcGetClock(&tm_datetime);
	
	setWDay(dayofweek(tm_datetime.tm_mday, tm_datetime.tm_mon+1, 2019));
}


/**
 * \brief sets hour of current time
 * 
 * \param hour
 * 
 * \return void
 */
void setHour(int hour)
{
	if (hour<0 || hour> 23)
	return;
	
	tm_datetime.tm_hour = hour;
	X12RtcSetClock(&tm_datetime);
}

/**
 * \brief sets minutes of current time
 * 
 * \param minutes
 * 
 * \return void
 */
void setMinutes(int minutes)
{
	if (minutes<0 || minutes> 59)
	return;
	
	tm_datetime.tm_min = minutes;
	X12RtcSetClock(&tm_datetime);
}

/**
 * \brief sets seconds of current time
 * 
 * \param seconds
 * 
 * \return void
 */
void setSeconds(int seconds)
{
	if(seconds < 0 || seconds > 59)
	return;
	
	tm_datetime.tm_sec = seconds;
	X12RtcSetClock(&tm_datetime);
}

/**
 * \brief sets time (hour & minutes) of current time
 * 
 * \param hour
 * \param minutes
 * 
 * \return void
 */
void setTime(int hour, int minutes)
{
	setHour(hour);
	setMinutes(minutes);
}

/**
 * \brief sets year of current time 
 * 
 * \param year
 * 
 * \return void
 */
void setYear(int year)
{
	year -= 100;
	
	tm_datetime.tm_year = year;
	X12RtcSetClock(&tm_datetime);
}

/**
 * \brief sets day of week of current time
 * 
 * \param wday
 * 
 * \return void
 */
void setWDay(int wday)
{
	tm_datetime.tm_wday = wday;
	X12RtcSetClock(&tm_datetime);
}

/**
 * \brief calculates the day of week
 * 
 * \param d day of month
 * \param m month of year
 * \param y year
 * 
 * \return day of week
 */
int dayofweek(int d, int m, int y)
{
	static int t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
	y -= m < 3;
	return ( y + y/4 - y/100 + y/400 + t[m-1] + d) % 7;
}

/**
 * \brief returns timezone to display
 * 
 * \param 
 * 
 * \return timezone
 */
double getTimeZone(void)
{
	return ((double)utcPlusTimeZone)/2.0;
}

/**
 * \brief sets time (hour & minutes) of current time using the timezone
 * 
 * \param hour
 * \param minutes
 * 
 * \return void
 */
void setTimeUsingTimezone(int hour, int minutes)
{
	if((utcPlusTimeZone % 2) == 1)
	{
		minutes += 30;
		
		if(minutes > 59)
		{
			minutes -= 60;
			hour++;
		}
		
		hour += ((utcPlusTimeZone-1)/2);
	}
	else
	{
		hour += (utcPlusTimeZone/2);
	}
	
	setTime(hour, minutes);
}

/**
 * \brief sets day oF month of current time
 * 
 * \param day day of month
 * 
 * \return void
 */
void setDay(int day)
{
	if (day<1 || day>31)
	return;
	
	tm_datetime.tm_mday = day;
	X12RtcSetClock(&tm_datetime);
}

/**
 * \brief sets month of current time 
 * 
 * \param month
 * 
 * \return void
 */
void setMonth(int month)
{	
	if (month<0 || month> 11)
	return;
	
	tm_datetime.tm_mon = month;
	X12RtcSetClock(&tm_datetime);
}

/**
 * \brief sets date (day & month) of current time 
 * 
 * \param day day of month
 * \param month
 * 
 * \return void
 */
void setDate(int day, int month)
{
	setDay(day);
	setMonth(month);
}


/**
 * \brief sets timezone, EXAMPLE: UK is UTC+01:00, so the param should be 1
 * 
 * \param utcPlus timezone
 * 
 * \return void
 */
void setTimeZone(int utcPlus) //Nog niet af
{
	utcPlusTimeZone = utcPlus;
	StorageSaveConfigT(&utcPlusTimeZone);
}

/**
 * \brief returns date string like 11/03
 * 
 * \param 
 * 
 * \return char* date string
 */
char* getDateString(void)
{
	X12RtcGetClock(&tm_datetime);
	
	char* string = malloc(sizeof(char) * (CHAR_ARRAY_SIZE_TIME + 1));
	
	sprintf(string, "%02d/%02d", tm_datetime.tm_mday, tm_datetime.tm_mon+1);
	
	return string;
}

/**
 * \brief returns time string like 13:34
 * 
 * \param 
 * 
 * \return char* time string
 */
char* getTimeString(void)
{
	X12RtcGetClock(&tm_datetime);
	
	char* string = malloc(sizeof(char) * (CHAR_ARRAY_SIZE_TIME + 1));
	
	sprintf(string, "%02d:%02d", tm_datetime.tm_hour, tm_datetime.tm_min);
	
	return string;
}

/**
 * \brief returms time strig like 13:34:30, this is the same as the getTimeString function but it also returns the seconds
 * 
 * \param 
 * 
 * \return char* time string
 */
char* getFullTimeString(void){
	X12RtcGetClock(&tm_datetime);
	
	char* string = malloc(sizeof(char) * (CHAR_ARRAY_SIZE_TIME + 1));
	
	sprintf(string, "%02d:%02d:%02d", tm_datetime.tm_hour, tm_datetime.tm_min, tm_datetime.tm_sec);

	return string;
}
