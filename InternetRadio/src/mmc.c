/* ========================================================================
 * [PROJECT]    SIR100
 * [MODULE]     MMC driver
 * [TITLE]      Media Card driver
 * [FILE]       mmc.c
 * [VSN]        1.0
 * [CREATED]    02 october 2006
 * [LASTCHNGD]  20 may 2007
 * [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
 * [PURPOSE]    routines and API to support MMC-application
 * ======================================================================== */

#define LOG_MODULE  LOG_MMC_MODULE

#include <string.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <fs/phatfs.h>
#include <dev/urom.h>
#include <dev/debug.h>

#include <sys/event.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/device.h>
#include <sys/bankmem.h>
#include <sys/heap.h>
#include <dev/board.h>

//#pragma text:appcode

#include "system.h"
#include "mmc.h"
#include "portio.h"
#include "vs10xx.h"
#include "display.h"
#include "log.h"
#include "fat.h"
#include "mmcdrv.h"
#include "led.h"
#include "keyboard.h"
#include "threadHandling.h"
#include "spidrv.h"
#include "player.h"
#include "UI.h"



#ifdef DEBUG
//#define MMC__DEBUG
#endif /* #ifdef DEBUG */
/*-------------------------------------------------------------------------*/
/* local defines                                                           */
/*-------------------------------------------------------------------------*/

#define CARD_PRESENT_COUNTER_OK         30
#define CARD_NOT_PRESENT_COUNTER_OK     20

#define UART_SETSPEED   0x0101


/*--------------------------------------------------------------------------*/
/*  Type declarations                                                       */
/*--------------------------------------------------------------------------*/
/*!\brief Statemachine for card-detection */
typedef enum T_CARD_STATE
{
    CARD_IDLE,                      /* nothing to do */
    CARD_PRESENT,                   /* card seen at least one time */
    CARD_VALID,                     /* card seen at least <valid> times */
    CARD_NOT_PRESENT                /* card not seen at least (valid> times */
}TCardState;


/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
static u_char CardPresentFlag;
static u_char ValidateCounter;

/*!\brief state-variable for Card-statemachine */
static TCardState CardState;

/*!\brief Status of this module */
static TError g_tStatus;

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static int PlayMp3Filex(char *path);


/*!
 * \addtogroup Card
 */

/*@{*/

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/*!
 * \brief check if MM-Card is inserted or removed.
 *
 * \Note: this routine is called from an ISR !
 *
 */
u_char CardCheckCard(void)
{
    u_char RetValue=CARD_NO_CHANGE;

    switch (CardState)
    {
        case CARD_IDLE:
            {
                if (bit_is_clear(MMC_IN_READ, MMC_CDETECT))
                {
                    ValidateCounter=1;
                    CardState = CARD_PRESENT;
                }
            }
            break;
        case CARD_PRESENT:
            {
                if (bit_is_clear(MMC_IN_READ, MMC_CDETECT))
                {
                    if (++ValidateCounter==CARD_PRESENT_COUNTER_OK)
                    {
                        CardPresentFlag=CARD_IS_PRESENT;
                        CardState=CARD_VALID;
                        RetValue=CARD_IS_PRESENT;
                    }
                }
                else
                {
                    CardState=CARD_IDLE;                  // false alarm,start over again
                }
            }
            break;
        case CARD_VALID:
            {
                if (bit_is_set(MMC_IN_READ, MMC_CDETECT))
                {
                    ValidateCounter=1;
                    CardState=CARD_NOT_PRESENT;         // Card removed
                }
            }
            break;
        case CARD_NOT_PRESENT:
            {
                if (++ValidateCounter==CARD_NOT_PRESENT_COUNTER_OK)
                {
                    CardPresentFlag=CARD_IS_NOT_PRESENT;
                    CardState=CARD_IDLE;
                    RetValue=CARD_IS_NOT_PRESENT;
                }
            }
            break;
    }
    return(RetValue);
}

/*!
 * \brief return status of "Card is Present"
 *
 */
u_char CardCheckPresent()
{
    return(CardPresentFlag);
}

/*!
 * \brief initialise the card by reading card contents (.pls files)
 *
 * We initialse the card by registering the card and the filesystem
 * that is on the card.
 *
 * Then we start checking if a number of playlists are
 * present on the card. The names of these playlists are hardcoded
 * (1.pls, 2.pls, to 20.pls). We 'search' the card for these list
 * of playlists by trying to open them. If succesfull, we read the
 * number of songs present (int) in that list
 * Finally we update some administration (global) variables
 *
 */
int CardInitCard()
{
    int iResult=-1;
    int fid;        // current file descriptor
    char szFileName[10];
	//int SETTINGS_NROF_PLAYLISTST = 100;
   // u_char i;
	u_char ief;
	NUTDEVICE *fsdev;
	int test = NutRegisterDevice(&devPhat0, 0, 0);
	printf("devPhat0 is : %d", test);
	for (fsdev = nutDeviceList; fsdev; fsdev = fsdev->dev_next) {
		printf(fsdev->dev_name);
		printf("\n");
		//if (fsdev->dev_name == 0) {
		//LogMsg_P(LOG_INFO, PSTR("test2"));//wel
		if (fsdev->dev_type == IFTYP_FS) {
			
			break;
		}
	}
	// }

    /*
     * Register our device for the file system (if not done already.....)
     */
    if (NutDeviceLookup(devFAT.dev_name) == 0)
    {
		printf("test1");
        ief = VsPlayerInterrupts(0);
        if ((iResult=NutRegisterDevice(&devFAT, FAT_MODE_MMC, 0)) == 0)
        {
					printf("test2");

            iResult=NutRegisterDevice(&devFATMMC0, FAT_MODE_MMC, 0);
			printf("iresult: %d", iResult);
        }
        VsPlayerInterrupts(ief);
    }
    else
    {
        NUTDEVICE * dev;
		printf("\ntest3");

        /*
         *  we must call 'FatInit' here to initialise and mount the filesystem (again)
         */

        FATRelease();
        ief = VsPlayerInterrupts(0);
        dev=&devFAT;
		
		//if (dev->dev_init != 0 || (*dev->dev_init)(dev) != 0)
		//{
			//printf("\ninit dev phat succes");
			//iResult = 0;
		//}
        if (dev->dev_init == 0 || (*dev->dev_init)(dev) == 0)
        {
            dev=&devFATMMC0;
            if (dev->dev_init == 0 || (*dev->dev_init)(dev) == 0)
            {
                iResult=0;
            }
        }
        VsPlayerInterrupts(ief);
		iResult = 0;
    }

    if (iResult==0)
    {
        LogMsg_P(LOG_INFO, PSTR("Card mounted"));
        /*
         *  try to open the playlists. If an error is returned, we assume the
         *  playlist does not exist and we do not check any further lists
         */
		//uint32_t baud = 115200;


		 SPIselect(SPI_DEV_MMC);
		 sprintf_P(szFileName, PSTR("FM0:1.mp3"), 1);
		 if ((fid = _open(szFileName, _O_RDONLY)) != -1)
		 {
			 printf("\nsucces SD card");

			 _close(fid);
			 FILE *streamLars = fopen(szFileName, "rb");
			 play(streamLars);
		 }
		 else
		 {
			 printf("\nfailed SD card");
		 }

		
		
		 /* Kroeske: onderstaande code ter illustratie om file op card te openen */
		 
		 //
		 //FILE *fp = fopen("PHAT0:/TEST.TXT", "w");
		 //printf("\nvoor file write");
		 //if (fp) {
		 //fprintf(fp, "HelloFile");
		 //fclose(fp);
		 //printf("succesfull");
		 //} else {
		 //printf("failed");
		 ///* File open failed. */
		 //}
		 //if (fp == NULL)
		 //{
		 //printf("fp is null");
		 //}
		 //printf("\nna file write");
		 
		 //FILE *stream = fopen(devPhat0.dev_name, "r+");
		 //_ioctl(_fileno(stream), UART_SETSPEED, &baud);
		 
		 //for (i=1; i<SETTINGS_NROF_PLAYLISTST; ++i)
		 //{
		 //printf("test1");
		 ////compose name to open
		 //SPIselect(SPI_DEV_MMC);
		 //sprintf_P(szFileName, PSTR("FM0:%d.pls"), 1);
		 //if ((fid = _open(szFileName, _O_RDONLY)) != -1)
		 //{
		 //printf("test1");
		 //
		 //_close(fid);
		 //FILE *streamLars = fopen(szFileName, "rb");
		 //}
		 //else
		 //{
		 //
		 ////g_NrofPlayLists=i-1;
		 //LogMsg_P(LOG_INFO, PSTR("Found %d Playlists on the Card"), i-1);
		 //break;
		 //}
		 //}
		 //}
		 //else
		 //{
		 //LogMsg_P(LOG_ERR, PSTR("Error initialising File system and Card-driver"));
		 //}
		 //
		 //return(iResult);
		 //}

	}
	return iResult;
}
THREAD(NyanThread, arg) //CustomThread
{ 
	for(;;) {
		VsSetVolume(0, 0);
		PlayMp3Filex("UROM:nyan.mp3");
		thread_handling_wait(50);
		if (closeBeeping)
		{
			NutThreadExit();
		}
	}
}


void playNyanCat(void){
	uint32_t baud = 115200;
	NutRegisterDevice(&devUrom, 0, 0);
    NutRegisterDevice(&devDebug0, 0, 0);

    /*
     * Assign stdout to the debug device.
     */
    freopen("uart0", "w", stdout);
    _ioctl(_fileno(stdout), UART_SETSPEED, &baud);

    /*
     * Print a banner.
     */
    //printf("\n\nPlay MP3 files on Nut/OS %s\n", NutVersionString());

    /*
     * Initialize the MP3 buffer. The NutSegBuf routines provide a global
     * system buffer, which works with banked and non-banked systems.
     */
    if (NutSegBufInit(8192) == 0) {
        puts("NutSegBufInit: Fatal error");
    }

    /* 
     * Initialize the MP3 decoder hardware.
     */
    if (VsPlayerInit() || VsPlayerReset(0)) {
        puts("VsPlayer: Fatal error");
    }

    /*
     * Play the MP3 files in an endless loop. For each file set the volume 
     * of the left and right channel, call the local routine PlayMp3File() 
     * and sleep one second before playing the next sound file.
     */
				NutThreadCreate("Bg", NyanThread, NULL, 512);

    //while(!closeBeeping) {
		        //VsSetVolume(0, 0);
		        //PlayMp3Filex("UROM:nyan.mp3");
		        //thread_handling_wait(50);
		        //printf("playing file nyan");
		
		//
        //VsSetVolume(0, 254);
        //PlayMp3Filex("UROM:sound1a.mp3");
        //thread_handling_wait(100);
		//printf("playing file 1");
        //VsSetVolume(0, 0);
        //PlayMp3Filex("UROM:sound2a.mp3");
        //thread_handling_wait(100);
		//printf("playing file 2");
//
        //VsSetVolume(254, 0);
        //PlayMp3Filex("UROM:sound3a.mp3");
        //thread_handling_wait(100);
		//printf("playing file 3");
//
        //VsSetVolume(0, 0);
        //PlayMp3Filex("UROM:sound4a.mp3");
       //thread_handling_wait(100);
	   		//printf("playing file 4");

   // }
	
}



/*
 * Play MP3 file from local file system.
 *
 * \param path Pathname of the MP3 file to play.
 *
 * \return 0 on success, -1 if opening the file failed.
 */
static int PlayMp3Filex(char *path)
{
    int fd;
    size_t rbytesx;
    u_char *mp3bufx;
    int got;
    u_char ief;

    /*
     * Open the MP3 file.
     */
    printf("Play %s: ", path);
    if((fd = _open(path, _O_RDONLY | _O_BINARY)) == -1) {
        printf("Error %d\n", errno);
        return -1;
    }
    puts("OK");

    /* 
     * Reset decoder buffer.
     */
    printf("[B.RST]");
    ief = VsPlayerInterrupts(0);
    NutSegBufReset();
    VsPlayerInterrupts(ief);

    for(;;) {
        /*
         * Query number of byte available in MP3 buffer.
         */
        ief = VsPlayerInterrupts(0);
	        mp3bufx = NutSegBufWriteRequest(&rbytesx);
        VsPlayerInterrupts(ief);

        /* 
         * Read data directly into the MP3 buffer. 
         */
        if(rbytesx) {
            printf("[B.RD%d]", rbytesx);
            if ((got = _read(fd, mp3bufx, rbytesx)) > 0) {
                printf("[B.CMT%d]", got);
                ief = VsPlayerInterrupts(0);
                mp3bufx = NutSegBufWriteCommit(got);
                VsPlayerInterrupts(ief);
            }
            else {
                printf("[EOF]");
                break;
            }
        }

        /*
         * If the player is not running, kick it.
         */
        if (VsGetStatus() != VS_STATUS_RUNNING) {
            printf("[P.KICK]");
            VsPlayerKick();
        }

        /*
         * Allow background threads to take over.
         */
        NutThreadYield();
    }

    _close(fd);

    /* 
     * Flush decoder and wait until finished. 
     */
    printf("[P.FLUSH]");
    VsPlayerKick();//VsPlayerFlush();
    while (VsGetStatus() == VS_STATUS_RUNNING) {
        thread_handling_wait(63);
    }

    /*
     * Reset the decoder. 
     */
    printf("[P.RST]");
    VsPlayerReset(0);

    printf("\nDone, %u bytes free\n", NutHeapAvailable());
    return 0;
}

		
/*!
 * \brief The CardPresent thread.
 *
 * execute code when card is inserted or redrawn
 *
 * \param   -
 *
 * \return  -
 */

THREAD(CardPresent, pArg)
{
    static u_char OldCardStatus;

    OldCardStatus=CardPresentFlag;

    for (;;)
    {
		thread_handling_wait(1);
		
        if ((CardPresentFlag==CARD_IS_PRESENT) && (OldCardStatus==CARD_IS_NOT_PRESENT))
        {
            LogMsg_P(LOG_INFO, PSTR("Card inserted"));
            if (CardInitCard()==0)
            {
                KbInjectKey(KEY_MMC_IN);
            }
            OldCardStatus=CardPresentFlag;
        }
        else if ((CardPresentFlag==CARD_IS_NOT_PRESENT) && (OldCardStatus==CARD_IS_PRESENT))
        {
            LogMsg_P(LOG_INFO, PSTR("Card removed"));
            CardClose();
            OldCardStatus=CardPresentFlag;
        }
        else
        {
            thread_handling_wait(500);
        }
    }
}


/*!
 * \brief return global variable that indicates the status of this module
 *
 */
TError CardStatus(void)
{
    return(g_tStatus);
}

/*!
 * \brief Stop playing.
 *
 * \param   -
 *
 * \return  -
 */
void CardClose(void)
{

}


/*!
 * \brief initialise this module
 *
 */
void CardInit()
{
    char ThreadName[10];

    CardState=CARD_IDLE;
    CardPresentFlag=CARD_IS_NOT_PRESENT;

    /*
     * Create a CardPresent thread
     */
    strcpy_P(ThreadName, PSTR("CardPres"));

    if (GetThreadByName((char *)ThreadName) == NULL)
    {
        if (NutThreadCreate((char *)ThreadName, CardPresent, 0, 768) == 0)
        {
            LogMsg_P(LOG_EMERG, PSTR("Thread failed"));
        }
    }

}

/* ---------- end of module ------------------------------------------------ */

/*@}*/


